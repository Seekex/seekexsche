package com.seekx.interfaces

interface AdapterListener {
    fun onSelection(any1: Any?, any2: Any?, i: Int)
}