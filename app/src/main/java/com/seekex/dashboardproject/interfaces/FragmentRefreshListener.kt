package com.seekx.interfaces

import android.widget.TextView

interface FragmentRefreshListener {
    fun onRefresh()
}