package com.seekex.dashboardproject

object AppConstants {


    const val SPACES =" "
    const val SPACES_DOTS =" . "
    const val isNull="null"

    const val EMPTY=""
    const val NA="NA"

    const val BASEURL="baseurl"
    const val LIVEURL="https://erp.sevenrocks.in/"
    const val TESTURL="https://test-live.sevenrocks.in/"
    const val STAGGINGURL="https://stagging.sevenrocks.in/"

    const val uid="uid"
    const val isLogin="isLogin"
    const val pic_url="pic_url"
    const val user_name="user_name"
    const val password="password"
    const val token="token"
    const val name="name"


}