package com.seekex.dashboardproject.models

import androidx.room.Ignore
import com.seekx.utils.ExtraUtils


class PieData(var one: Float,
              var two: Float,
              var three: Float){

    @Ignore
    constructor():this(0f,0f,0f)

    fun  getTotal():String{
        return ExtraUtils.roundTwoDecimals((one+two+three).toDouble()).toString()
    }
    fun  getTotal2():String{
        return ExtraUtils.getFormatedNumber((one+two+three).toDouble()).toString()
    }
}