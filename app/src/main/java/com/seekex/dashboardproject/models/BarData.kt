package com.seekex.dashboardproject.models

import androidx.room.Ignore
import com.seekx.utils.ExtraUtils


class BarData(var xvalues: Float,
              var yvalues: Float){

    @Ignore
    constructor():this(0f,0f)

}