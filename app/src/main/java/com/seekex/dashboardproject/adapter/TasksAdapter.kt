package com.seekex.dashboardproject.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.seekex.dashboardproject.Preferences
import com.seekex.dashboardproject.databinding.TaskAdapterBinding
import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.TaskResponse2
import com.seekx.interfaces.AdapterListener
import com.seekx.interfaces.DialogeUtilsCallBack
import com.seekx.utils.DialogUtils
import com.seekx.webService.ApiImp
import java.text.SimpleDateFormat
import java.util.*


class TasksAdapter(
    private val dialogUtils: DialogUtils,
    private val apiImp: ApiImp,
    private val context: Context,
    private val pref: Preferences,
    private val adapterListener: AdapterListener
) :
    RecyclerView.Adapter<TasksAdapter.ViewHolder>() {

    private var items: MutableList<TaskResponse2> = arrayListOf()


    fun setDataValues(items: MutableList<TaskResponse2>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = TaskAdapterBinding.inflate(inflater)

        binding.llShowntimer.setOnClickListener {
            adapterListener.onSelection(binding.model!!, "stop", 0)



        }
        binding.llHiddentimer.setOnClickListener {


            if (pref.getBoolean("isTaskStatus")) {

                dialogUtils.showAlertWithCallBack(
                    "You already have task Started. To Start new you have to stop previous one, Do you want to continue ?",
                    "Continue",
                    "Cancel",
                    object : DialogeUtilsCallBack {
                        override fun onDoneClick(clickStatus: Boolean) {
                            if (clickStatus) {
//                                apiImp.showProgressBar()
                                adapterListener.onSelection(binding.model!!, "start", 0)
//                                hitTimerApi(actReq, binding?.model!!)

                            }

                        }

                    })

            } else {
//                apiImp.showProgressBar()
                adapterListener.onSelection(binding.model!!, "start", 0)

//                hitTimerApi(actReq, binding?.model!!)
            }

//
        }


        return ViewHolder(binding)
    }

    private fun getTimeInMillis(): Long {
        val formatter = SimpleDateFormat("dd.MM.yyyy, hh:mm")
        formatter.isLenient = false
        val sdf = SimpleDateFormat("dd.MM.yyyy, hh:mm")

        val curDate = Date()
        val curMillis = curDate.time
        val curTime = formatter.format(curDate)
        val mDate: Date = sdf.parse(curTime)
        val timeInMilliseconds = mDate.time

        return timeInMilliseconds
    }




    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ViewHolder(private val binding: TaskAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: TaskResponse2) {


            binding.model = item
            binding.executePendingBindings()
        }
    }

    fun getFormatTime(): String? {
        val c: Calendar = Calendar.getInstance()
        val dateformat =
            SimpleDateFormat("hh:mm") //it will give you the date in the formate that is given in the image
        return dateformat.format(c.getTime())
    }


}