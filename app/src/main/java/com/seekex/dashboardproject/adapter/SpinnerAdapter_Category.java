package com.seekex.dashboardproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.seekex.dashboardproject.R;
import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.GetCategoryResponse2;

import java.util.ArrayList;

public class SpinnerAdapter_Category extends ArrayAdapter<GetCategoryResponse2> {

    public SpinnerAdapter_Category(Context context,
                                   ArrayList<GetCategoryResponse2> algorithmList)
    {
        super(context, 0, algorithmList);
    }
  
    @NonNull
    @Override
    public View getView(int position, @Nullable
            View convertView, @NonNull ViewGroup parent)
    {
        return initView(position, convertView, parent);
    }
  
    @Override
    public View getDropDownView(int position, @Nullable
                                              View convertView, @NonNull ViewGroup parent)
    {
        return initView(position, convertView, parent);
    }
  
    private View initView(int position, View convertView,
                          ViewGroup parent)
    {
        // It is used to set our custom view.
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.spinner_item, parent, false);
        }

        TextView textViewName = null;
        GetCategoryResponse2 currentItem = null;
        try {
            textViewName = convertView.findViewById(R.id.text_view);
            currentItem = getItem(position);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // It is used the name to the TextView when the
        // current item is not null.
        if (currentItem != null) {
            textViewName.setText(currentItem.getName());
        }
        return convertView;
    }
}