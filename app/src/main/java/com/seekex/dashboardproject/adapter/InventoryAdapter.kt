package com.seekex.dashboardproject.adapter

import android.content.Context
import android.util.Log
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.seekex.dashboardproject.fragments.invnetory.*

class InventoryAdapter(
    private val myContext: Context,
    fm: FragmentManager,
    internal var totalTabs: Int
) :
    FragmentPagerAdapter(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private var mCurrentFragment: Fragment? = null

    // this is for fragment tabs
    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {
                return BarChartFragment()
            }
            1 -> {
                return BarChartFragmentOut()
            }
            2 -> {
                return TypeWisePieData()
            }
            3 -> {
                return TypeWiseCompleteData()
            }
            4 -> {
                return TopIncomingProductPieData()
            }
            5 -> {
                return TopOutgoingProductPieData()
            }
            6 -> {
                return BarINventoryAuditFragment()
            }
            7 -> {
                return BarINventoryPIckPackFragment()
            }
            8 -> {
                return BarBinAuditCountFagment()
            }
            9 -> {
                return OrderReturnWiseFagment()
            }
            10 -> {
                return OutgoingPiece_partywise()
            }
            11 -> {
                return OutgoingKg_partywise()
            }
            12 -> {
                return GradeMovementFragment()
            }
            13 -> {
                return ScannableLostMarkFragment()
            }
            else -> return OrderReturnWiseFagment()
        }
    }

    fun getCurrentFragment(): Fragment? {
        Log.e("TAG", "getCurrentFragment: " + mCurrentFragment)
        return mCurrentFragment
    }

    override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {

        if (getCurrentFragment() !== `object`) {
            mCurrentFragment = `object` as Fragment
        }
        super.setPrimaryItem(container, position, `object`)
    }


    // this counts total number of tabs
    override fun getCount(): Int {
        return totalTabs
    }
}  