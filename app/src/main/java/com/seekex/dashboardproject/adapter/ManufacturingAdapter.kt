package com.seekex.dashboardproject.adapter

import android.content.Context
import android.util.Log
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.seekex.dashboardproject.fragments.invnetory.*
import com.seekex.dashboardproject.fragments.manufacturing.ManufacturingBarFragment

class ManufacturingAdapter(
    private val myContext: Context,
    fm: FragmentManager,
    internal var totalTabs: Int
) :
    FragmentPagerAdapter(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private var mCurrentFragment: Fragment? = null

    // this is for fragment tabs
    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {
                return ManufacturingBarFragment()
            }

            else -> return ManufacturingBarFragment()
        }
    }

    fun getCurrentFragment(): Fragment? {
        Log.e("TAG", "getCurrentFragment: " + mCurrentFragment)
        return mCurrentFragment
    }

    override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {

        if (getCurrentFragment() !== `object`) {
            mCurrentFragment = `object` as Fragment
        }
        super.setPrimaryItem(container, position, `object`)
    }


    // this counts total number of tabs
    override fun getCount(): Int {
        return totalTabs
    }
}  