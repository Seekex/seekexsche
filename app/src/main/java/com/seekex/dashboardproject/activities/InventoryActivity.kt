package com.seekex.dashboardproject.activities

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.seekex.dashboardproject.AppConstants
import com.seekex.dashboardproject.Preferences
import com.seekex.dashboardproject.R
import com.seekex.dashboardproject.adapter.InventoryAdapter
import com.seekex.dashboardproject.fragments.*
import com.seekex.dashboardproject.fragments.invnetory.*
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.SearchInvenDTO
import com.seekx.utils.DataUtils
import com.seekx.utils.DialogUtils
import com.seekx.webService.ApiImp
import kotlinx.android.synthetic.main.filter_view.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.header.view.*
import java.util.*


class InventoryActivity : AppCompatActivity() {


    private lateinit var adapter: InventoryAdapter

    var tabLayout: TabLayout? = null
    var viewPager: ViewPager? = null
    lateinit var pref: Preferences
    lateinit var apiImp: ApiImp
    lateinit var dialogUtils: DialogUtils
    override fun onBackPressed() {
        dialog.dismiss()

        super.onBackPressed()
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSearchData()

        dialogUtils = DialogUtils(this)
        apiImp = ApiImp(this)
        pref = Preferences(this)
        tabLayout = findViewById<TabLayout>(R.id.tabLayout)
        viewPager = findViewById<ViewPager>(R.id.viewPager)

        tabLayout!!.addTab(tabLayout!!.newTab().setText("Inventory Out"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Inventory Int"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Type-Wise"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Complete-Wise"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Top Incoming Product"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Top Outgoing Product"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Bin Audit"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Pick/Pack Dispatch"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Bin audit Count"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Order Return Type"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Outgoing In Piece-Party wise"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Outgoing In KG-Party wise"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Grade Movement"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Scannable Lost Mark"))
        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL

        adapter = InventoryAdapter(this, supportFragmentManager, tabLayout!!.tabCount)
        viewPager!!.adapter = adapter

        viewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        Objects.requireNonNull(tabLayout!!.getTabAt(0))?.select();
        img_search.visibility = View.VISIBLE

        header.setText("Inventory data")

        img_search.setOnClickListener {
            initializePopUp()
        }

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager!!.currentItem = tab.position

            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
        tabLayout!!.setOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                selectTab(tab)
            }

            private fun selectTab(tab: TabLayout.Tab) {
                // do something
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
                if (tab.getPosition() === 0) {
                    selectTab(tab)
                    tabLayout!!.setOnTabSelectedListener(object : OnTabSelectedListener {
                        override fun onTabSelected(tab: TabLayout.Tab) {
                            selectTab(tab)
                        }

                        override fun onTabReselected(arg0: TabLayout.Tab?) {}
                        override fun onTabUnselected(arg0: TabLayout.Tab?) {}
                    })
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {}
        })
    }

    private fun setSearchData() {
        searchDto.from_date = "2020-06-1"
        searchDto.to_date = "2020-06-30"
        searchDto.interval = "day"
    }

    lateinit var dialog: Dialog

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun initializePopUp() {
        dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.filter_view)
        dialog.show()
        dialog.setCanceledOnTouchOutside(true)
        initializeClickListener()
        dialog.ll_category.visibility = View.GONE

        hideInterval()

    }

    private fun hideInterval() {
        var ss = adapter.getCurrentFragment()
        when (ss) {
            is OutgoingPiece_partywise -> {
                dialog.ll_category.visibility = View.GONE
                dialog.ll_interval.visibility = View.GONE
            }
            is OutgoingKg_partywise -> {
                dialog.ll_category.visibility = View.GONE
                dialog.ll_interval.visibility = View.GONE
            }
        }    }

    private val cal = Calendar.getInstance()

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun initializeClickListener() {


        try {
            dialog.spin_range.setOnItemSelectedListener(object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?, view: View,
                    position: Int, id: Long
                ) {
                    if (adapterView!!.getItemAtPosition(position).toString().equals("Today")) {
                        searchDto.from_date = DataUtils.getCurrentDate()
                        searchDto.to_date = DataUtils.getCurrentDate()
                    } else if (adapterView!!.getItemAtPosition(position).toString()
                            .equals("Yesterday")
                    ) {
                        searchDto.from_date = DataUtils.getYesterdayDate()
                        searchDto.to_date = DataUtils.getCurrentDate()
                    } else if (adapterView!!.getItemAtPosition(position).toString()
                            .equals("3 days before today")
                    ) {
                        searchDto.from_date = DataUtils.getthreeDaysDate()
                        searchDto.to_date = DataUtils.getCurrentDate()
                    } else if (adapterView!!.getItemAtPosition(position).toString()
                            .equals("7 days before today")
                    ) {
                        searchDto.from_date = DataUtils.getsevenDaysDate()
                        searchDto.to_date = DataUtils.getCurrentDate()
                    } else if (adapterView!!.getItemAtPosition(position).toString()
                            .equals("Current Month")
                    ) {
                        searchDto.from_date = DataUtils.getCurrentMonthFirstDate()
                        searchDto.to_date = DataUtils.getCurrentMonthLastDate()
                    } else if (adapterView!!.getItemAtPosition(position).toString()
                            .equals("Last 30 days")
                    ) {
                        searchDto.from_date = DataUtils.getLastThirtyDaysStartDate()
                        searchDto.to_date = DataUtils.getCurrentDate()
                    } else if (adapterView!!.getItemAtPosition(position).toString()
                            .equals("Last Month")
                    ) {
                        searchDto.from_date = DataUtils.getLastMonthStartDate()
                        searchDto.to_date = DataUtils.getLastMonthLastDate()
                    }

                    Log.e(
                        "TAG",
                        "onItemSelected: " + searchDto.from_date + " -- " + searchDto.to_date
                    )
                }

                override fun onNothingSelected(adapter: AdapterView<*>?) {}
            })
        } catch (e: Exception) {
        }
        try {
            dialog.spin_interval.setOnItemSelectedListener(object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?, view: View,
                    position: Int, id: Long
                ) {
                    searchDto.interval = adapterView!!.getItemAtPosition(position).toString()
                }

                override fun onNothingSelected(adapter: AdapterView<*>?) {}
            })
        } catch (e: Exception) {
        }
        dialog.btn_customdate.setOnClickListener {

            dialog.ll_datelayout.visibility = View.VISIBLE

        }
        dialog.bt_reset.setOnClickListener {

            dialog.spin_range.setSelection(0)
            dialog.spin_interval.setSelection(0)
            dialog.edt_fromdate.setText("")
            dialog.edt_todate.setText("")

        }
        dialog.bt_save.setOnClickListener {

            var ss = adapter.getCurrentFragment()

            if (searchDto.interval.equals("Select Interval")) {
                Toast.makeText(this, "Select Interval", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (searchDto.from_date.equals("") || searchDto.to_date.equals("")) {
                Toast.makeText(this, "Select Range or Custom date", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            when (ss) {
                is BarChartFragment -> {
                    ss.getAllapi()
                }
                is BarChartFragmentOut -> {
                    ss.getAllapi()
                }
                is TypeWisePieData -> {
                    ss.getPiedata()
                }
                is TypeWiseCompleteData -> {
                    ss.getPiedata()
                }
                is TopIncomingProductPieData -> {
                    ss.getPiedata()
                }
                is TopOutgoingProductPieData -> {
                    ss.getPiedata()
                }
                is BarINventoryAuditFragment -> {
                    ss.getAllapi()
                }
                is BarINventoryPIckPackFragment -> {
                    ss.getAllapi()
                }
                is BarBinAuditCountFagment -> {
                    ss.getAllapi()
                }
                is OrderReturnWiseFagment -> {
                    ss.getAllapi()
                }
                is OutgoingPiece_partywise -> {
                    ss.getAllapi()
                }
                is OutgoingKg_partywise -> {
                    ss.getAllapi()
                }
                is GradeMovementFragment -> {
                    ss.getAllapi()
                }
                is ScannableLostMarkFragment -> {
                    ss.getAllapi()
                }

            }

            dialog.dismiss()
        }

        dialog.edt_fromdate.setOnClickListener {
            val dpd = DatePickerDialog(this, { _, year, month, date ->
                val dob = String.format("%02d", date) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%d", year)
                var startDate = String.format("%d", year) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%02d", date)
                dialog.edt_fromdate.setText(dob)
                searchDto.from_date = startDate
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE))
            dpd.datePicker.maxDate = System.currentTimeMillis() - 1000
            dpd.show()
        }
        dialog.edt_todate.setOnClickListener {
            val dpd = DatePickerDialog(this, { _, year, month, date ->
                val dob = String.format("%02d", date) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%d", year)
                var startDate = String.format("%d", year) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%02d", date)
                dialog.edt_todate.setText(dob)
                searchDto.to_date = startDate
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE))
            dpd.datePicker.maxDate = System.currentTimeMillis() - 1000
            dpd.show()
        }
    }

    fun logout() {
        pref.set(AppConstants.uid, "")
        pref.setBoolean(AppConstants.isLogin, false)

        val i = Intent(this, LoginActivity::class.java)
// set the new task and clear flags
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(i)
    }

    companion object {


        @JvmStatic
        var searchDto =
            SearchInvenDTO()
    }
}