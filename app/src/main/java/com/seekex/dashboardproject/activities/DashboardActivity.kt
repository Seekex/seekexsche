package com.seekex.dashboardproject.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.seekex.dashboardproject.AppConstants
import com.seekex.dashboardproject.Preferences
import com.seekex.dashboardproject.R
import com.seekx.webService.ApiImp
import kotlinx.android.synthetic.main.dashboard_main.*
import kotlinx.android.synthetic.main.header.*
import java.util.*


class DashboardActivity : AppCompatActivity() {
    lateinit var pref: Preferences
    lateinit var apiImp: ApiImp
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dashboard_main)
        apiImp = ApiImp(this)
        pref = Preferences(this)

        btn_inventory.setOnClickListener {
            val i = Intent(this@DashboardActivity, InventoryActivity::class.java)
            startActivity(i)
        }
        btn_manufacturing.setOnClickListener {
            val i = Intent(this@DashboardActivity, NonScanableInventoryActivity::class.java)
            startActivity(i)
        }
        logout.visibility= View.VISIBLE

        logout.setOnClickListener {
            logout()
        }

    }

    fun logout() {
        pref.set(AppConstants.uid, "")
        pref.setBoolean(AppConstants.isLogin, false)

        val i = Intent(this, LoginActivity::class.java)
// set the new task and clear flags
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(i)
    }

}