package com.seekex.dashboardproject.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.seekex.dashboardproject.AppConstants
import com.seekex.dashboardproject.Preferences
import com.seekex.dashboardproject.R
import com.seekx.utils.*
import kotlinx.android.synthetic.main.chooseactivity.*
import kotlinx.android.synthetic.main.loginactivity.*
import kotlinx.android.synthetic.main.loginactivity.view.*
import java.util.*

class ChooseActivity : AppCompatActivity() {
    lateinit var pref: Preferences
    lateinit var dialogUtils: DialogUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.chooseactivity)

        init()
        setListener()

    }

    private fun init() {
        dialogUtils = DialogUtils(this)
        pref = Preferences(this)

    }


    private fun setListener() {
        chk_live.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                chk_stagging.isChecked=false
                chk_test.isChecked=false
                pref.set(AppConstants.BASEURL,AppConstants.LIVEURL)
            }

        }
        chk_test.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                chk_stagging.isChecked=false
                chk_live.isChecked=false
                Toast.makeText(this, isChecked.toString(), Toast.LENGTH_SHORT).show()
                pref.set(AppConstants.BASEURL,AppConstants.TESTURL)
            }


        }
        chk_stagging.setOnCheckedChangeListener { buttonView, isChecked ->
            chk_live.isChecked=false
            chk_test.isChecked=false
            pref.set(AppConstants.BASEURL,AppConstants.STAGGINGURL)
        }
        btn_next.setOnClickListener{
            val i = Intent(this@ChooseActivity, LoginActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

            startActivity(i)
            finish()
        }

    }


}