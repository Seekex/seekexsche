package com.seekex.dashboardproject.activities

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.seekex.dashboardproject.AppConstants
import com.seekex.dashboardproject.Preferences
import com.seekex.dashboardproject.R
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.ApiCallBacknew
import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.LoginResponse
import com.seekex.dashboardproject.utils.ActivityUtils
import com.seekx.utils.*
import com.seekx.webService.ApiImp
import com.seekx.webService.ApiUtils
import com.seekx.webService.models.apiRequest.LoginReq
import kotlinx.android.synthetic.main.loginactivity.*
import kotlinx.android.synthetic.main.loginactivity.view.*
import java.util.*

class LoginActivity : AppCompatActivity(), ApiCallBacknew {
    lateinit var pref: Preferences
    lateinit var apiImp: ApiImp
    lateinit var dialogUtils: DialogUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loginactivity)

        init()
        setListener()

    }

    private fun init() {
        dialogUtils = DialogUtils(this)
        apiImp = ApiImp(this)
        pref = Preferences(this)
        apiImp.setApiCallBacknew(this)

    }


    private fun setListener() {

        login_button.setOnClickListener {
            apiImp?.showProgressBar()
            if (check())
                validateUser(
                    ExtraUtils.getVal(et_username),
                    ExtraUtils.getVal(et_pass)
                )
            else
                apiImp?.cancelProgressBar()
        }


    }

    fun validateUser(username: String, pass: String) {
        val loginReq = LoginReq()

        loginReq.username = username
        loginReq.password = pass
        loginReq.setService_name(ApiUtils.LOGIN)

        Log.e("TAG", "validateUser: " + username)
        apiImp.login(loginReq)

    }

    override fun onResume() {
        super.onResume()
    }

    private fun check(): Boolean {
        return when {
            ValidationUtils.isEmpty(et_username) -> {
                dialogUtils.showAlert(getString(R.string.error_username))
                false
            }
            ValidationUtils.isContainsSpace(et_username) -> {
                dialogUtils.showAlert(getString(R.string.error_username_space))
                false
            }
            ValidationUtils.isEmpty(et_pass) -> {
                dialogUtils.showAlert(getString(R.string.err_password))
                false
            }
            else -> true
        }

    }

    override fun onSuccess(serviceName: String?, `object`: Any?) {
        val detail = `object` as LoginResponse
        pref.set(AppConstants.uid, detail.user.id!!.toString())
        pref.set(AppConstants.pic_url, detail.user.profile_image_thumbnail)
        pref.set(AppConstants.name, detail.user.name)
        pref.setBoolean(AppConstants.isLogin, true)
        pref.set(AppConstants.user_name, et_username.text.toString())
        pref.set(AppConstants.password, et_pass.text.toString())
        pref.set(AppConstants.token, detail.token)
        ActivityUtils.navigate(this@LoginActivity, DashboardActivity::class.java, false)

    }

    override fun onFailed(msg: String?) {
        TODO("Not yet implemented")
    }
}