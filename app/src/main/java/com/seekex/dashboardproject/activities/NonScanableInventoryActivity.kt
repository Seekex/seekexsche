package com.seekex.dashboardproject.activities

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.seekex.dashboardproject.AppConstants
import com.seekex.dashboardproject.Preferences
import com.seekex.dashboardproject.R
import com.seekex.dashboardproject.adapter.NonScanableInventoryAdapter
import com.seekex.dashboardproject.adapter.SpinnerAdapter_Category
import com.seekex.dashboardproject.fragments.*
import com.seekex.dashboardproject.fragments.invnetory.*
import com.seekex.dashboardproject.fragments.nonscanableinventory.CurrentInventorySnapshotFragment
import com.seekex.dashboardproject.fragments.nonscanableinventory.NonScanableInventoryInFragment
import com.seekex.dashboardproject.fragments.nonscanableinventory.NonScanableInventoryOutFragment
import com.seekex.dashboardproject.fragments.nonscanableinventory.SubLocationTransactionFragment
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.*
import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.*
import com.seekx.utils.DataUtils
import com.seekx.utils.DialogUtils
import com.seekx.webService.ApiImp
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import kotlinx.android.synthetic.main.filter_view.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.header.view.*
import kotlinx.android.synthetic.main.homeactivity.view.*
import java.util.*


class NonScanableInventoryActivity : AppCompatActivity() {

    private var categoryId: String? = ""
    private lateinit var adapterCity: SpinnerAdapter_Category

    private lateinit var adapter: NonScanableInventoryAdapter

    var tabLayout: TabLayout? = null
    var viewPager: ViewPager? = null
    lateinit var pref: Preferences
    lateinit var apiImp: ApiImp
    lateinit var dialogUtils: DialogUtils
    var cityList: ArrayList<GetCategoryResponse2> = java.util.ArrayList()

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSearchData()

        dialogUtils = DialogUtils(this)
        apiImp = ApiImp(this)

        pref = Preferences(this)
        tabLayout = findViewById<TabLayout>(R.id.tabLayout)
        viewPager = findViewById<ViewPager>(R.id.viewPager)

        tabLayout!!.addTab(tabLayout!!.newTab().setText("Non Scanable Inventory Out"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Non Scanable Inventory In"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Sub Location Transaction"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Current Inventory Snapshot"))

        adapter = NonScanableInventoryAdapter(this, supportFragmentManager, tabLayout!!.tabCount)
        viewPager!!.adapter = adapter

        viewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        Objects.requireNonNull(tabLayout!!.getTabAt(0))?.select();
        img_search.visibility = View.VISIBLE

        header.setText("Inventory data")

        img_search.setOnClickListener {
            initializePopUp()
        }

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager!!.currentItem = tab.position

            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
        tabLayout!!.setOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                selectTab(tab)
            }

            private fun selectTab(tab: TabLayout.Tab) {
                // do something
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
                if (tab.getPosition() === 0) {
                    selectTab(tab)
                    tabLayout!!.setOnTabSelectedListener(object : OnTabSelectedListener {
                        override fun onTabSelected(tab: TabLayout.Tab) {
                            selectTab(tab)
                        }

                        override fun onTabReselected(arg0: TabLayout.Tab?) {}
                        override fun onTabUnselected(arg0: TabLayout.Tab?) {}
                    })
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {}
        })
    }

    private fun setSearchData() {
        searchDto.from_date = "2020-06-1"
        searchDto.to_date = "2021-06-30"
        searchDto.interval = "month"
        searchDto.category_id = "5"
    }

    lateinit var dialog: Dialog

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun initializePopUp() {
        dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.filter_view)
        dialog.show()
        dialog.setCanceledOnTouchOutside(true)
        initializeClickListener()


        adapterCity =
            SpinnerAdapter_Category(
                this,
                cityList
            )

        dialog.spin_category.setAdapter(adapterCity)

        getCategoryList()

    }

    private val cal = Calendar.getInstance()

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun initializeClickListener() {
        try {
            dialog.spin_category.setOnItemSelectedListener(object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?, view: View,
                    position: Int, id: Long
                ) {
                    val user: GetCategoryResponse2 = adapterCity.getItem(position)!!
                    searchDto.category_id = user.id
                }

                override fun onNothingSelected(adapter: AdapterView<*>?) {}
            })
        } catch (e: Exception) {
        }
        try {
            dialog.spin_range.setOnItemSelectedListener(object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?, view: View,
                    position: Int, id: Long
                ) {
                    if (adapterView!!.getItemAtPosition(position).toString().equals("Today")) {
                        searchDto.from_date = DataUtils.getCurrentDate()
                        searchDto.to_date = DataUtils.getCurrentDate()
                    } else if (adapterView!!.getItemAtPosition(position).toString()
                            .equals("Yesterday")
                    ) {
                        searchDto.from_date = DataUtils.getYesterdayDate()
                        searchDto.to_date = DataUtils.getCurrentDate()
                    } else if (adapterView!!.getItemAtPosition(position).toString()
                            .equals("3 days before today")
                    ) {
                        searchDto.from_date = DataUtils.getthreeDaysDate()
                        searchDto.to_date = DataUtils.getCurrentDate()
                    } else if (adapterView!!.getItemAtPosition(position).toString()
                            .equals("7 days before today")
                    ) {
                        searchDto.from_date = DataUtils.getsevenDaysDate()
                        searchDto.to_date = DataUtils.getCurrentDate()
                    } else if (adapterView!!.getItemAtPosition(position).toString()
                            .equals("Current Month")
                    ) {
                        searchDto.from_date = DataUtils.getCurrentMonthFirstDate()
                        searchDto.to_date = DataUtils.getCurrentMonthLastDate()
                    } else if (adapterView!!.getItemAtPosition(position).toString()
                            .equals("Last 30 days")
                    ) {
                        searchDto.from_date = DataUtils.getLastThirtyDaysStartDate()
                        searchDto.to_date = DataUtils.getCurrentDate()
                    } else if (adapterView!!.getItemAtPosition(position).toString()
                            .equals("Last Month")
                    ) {
                        searchDto.from_date = DataUtils.getLastMonthStartDate()
                        searchDto.to_date = DataUtils.getLastMonthLastDate()
                    }

                    Log.e(
                        "TAG",
                        "onItemSelected: " + searchDto.from_date + " -- " + searchDto.to_date
                    )
                }

                override fun onNothingSelected(adapter: AdapterView<*>?) {}
            })
        } catch (e: Exception) {
        }
        try {
            dialog.spin_interval.setOnItemSelectedListener(object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?, view: View,
                    position: Int, id: Long
                ) {
                    searchDto.interval = adapterView!!.getItemAtPosition(position).toString()
                }

                override fun onNothingSelected(adapter: AdapterView<*>?) {}
            })
        } catch (e: Exception) {
        }
        dialog.btn_customdate.setOnClickListener {

            dialog.ll_datelayout.visibility = View.VISIBLE

        }
        dialog.bt_reset.setOnClickListener {
            dialog.spin_range.setSelection(0)
            dialog.spin_interval.setSelection(0)
            dialog.edt_fromdate.setText("")
            dialog.edt_todate.setText("")

        }
        dialog.bt_save.setOnClickListener {

            var ss = adapter.getCurrentFragment()

            if (searchDto.interval.equals("Select Interval")) {
                Toast.makeText(this, "Select Interval", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (searchDto.from_date.equals("") || searchDto.to_date.equals("")) {
                Toast.makeText(this, "Select Range or Custom date", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (searchDto.category_id.equals("")) {
                Toast.makeText(this, "Select Category", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            when (ss) {
                is NonScanableInventoryInFragment -> {
                    ss.getAllapi()
                }
                is NonScanableInventoryOutFragment -> {
                    ss.getAllapi()
                }
                is CurrentInventorySnapshotFragment -> {
                    ss.getAllapi()
                }
                is SubLocationTransactionFragment -> {
                    ss.getAllapi()
                }


            }

            dialog.dismiss()
        }

        dialog.edt_fromdate.setOnClickListener {
            val dpd = DatePickerDialog(this, { _, year, month, date ->
                val dob = String.format("%02d", date) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%d", year)
                var startDate = String.format("%d", year) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%02d", date)
                dialog.edt_fromdate.setText(dob)
                searchDto.from_date = startDate
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE))
            dpd.datePicker.maxDate = System.currentTimeMillis() - 1000
            dpd.show()
        }
        dialog.edt_todate.setOnClickListener {
            val dpd = DatePickerDialog(this, { _, year, month, date ->
                val dob = String.format("%02d", date) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%d", year)
                var startDate = String.format("%d", year) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%02d", date)
                dialog.edt_todate.setText(dob)
                searchDto.to_date = startDate
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE))
            dpd.datePicker.maxDate = System.currentTimeMillis() - 1000
            dpd.show()
        }
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getCategoryList() {
        apiImp.showProgressBar()

        val inveReq =
            GetCategoryReq()
        val inveReq2 =
            GetCategoryReq2()

        inveReq2.is_finish_good = "0"

        inveReq.setService_name(ApiUtils.GETCATEGORYLIST)
        inveReq.token = pref.get(AppConstants.token)
        inveReq.conditions = inveReq2

        apiImp.getCategoryList(inveReq, object: ApiCallBack{
            override fun onSuccess(status: Boolean, any: Any) {
                Log.e("getCategoryList", "onSuccess: ")

                apiImp.cancelProgressBar()
                val detail = any as GetCategoryResponse
                var aa =
                    GetCategoryResponse2()
                aa.id = "0"
                aa.name = "Select Category"

                cityList.add(aa)
                cityList.addAll(detail.data)

                adapterCity.notifyDataSetChanged()
            }

        })
    }



    fun logout() {
        pref.set(AppConstants.uid, "")
        pref.setBoolean(AppConstants.isLogin, false)

        val i = Intent(this, LoginActivity::class.java)
// set the new task and clear flags
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(i)
    }

    companion object {


        @JvmStatic
        var searchDto =
            SearchInvenDTO()
    }
}