package com.seekex.dashboardproject.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.seekex.dashboardproject.AppConstants
import com.seekex.dashboardproject.Preferences
import com.seekex.dashboardproject.R

class Splash : AppCompatActivity() {
    lateinit var pref: Preferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splashactivity)
        pref = Preferences(this)


        Handler().postDelayed({ // This method will be executed once the timer is over

            if (pref.getBoolean(AppConstants.isLogin)){
                val i = Intent(this@Splash, DashboardActivity::class.java)
                startActivity(i)
                finish()
            }else{
                val i = Intent(this@Splash, ChooseActivity::class.java)
                startActivity(i)
                finish()
            }

        }, 3000)



    }
}