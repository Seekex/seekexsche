package com.seekex.dashboardproject.custombinding

import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.SystemClock
import android.text.TextUtils
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.seekex.dashboardproject.R
import com.seekex.dashboardproject.custom.Fill
import com.seekex.dashboardproject.custom.MyAxisValueFormatter
import com.seekex.dashboardproject.models.BarData
import com.seekex.dashboardproject.models.PieData
import com.seekx.utils.ExtraUtils
import com.seekx.utils.ImageUtils
import kotlinx.android.synthetic.main.homeactivity.view.*

class CustonViewBindings {

    companion object {
        @JvmStatic
        @BindingAdapter("setPieChart")
        fun setPieChart(chart: PieChart?, data: PieData?) {

            if (chart == null || data == null)
                return

            if (data.getTotal() == "0")
                return

            chart.run {

                setUsePercentValues(true)
                description.isEnabled = false
                setExtraOffsets(5f, 10f, 5f, 5f)
                dragDecelerationFrictionCoef = 0.95f

                setCenterTextTypeface(
                    Typeface.createFromAsset(
                        context.getAssets(),
                        "montserratmed.ttf"
                    )
                )

                setExtraOffsets(20f, 0f, 20f, 0f)

                isDrawHoleEnabled = true
                setHoleColor(Color.WHITE)

                setTransparentCircleColor(Color.WHITE)
                setTransparentCircleAlpha(110)

                holeRadius = 50f
                transparentCircleRadius = 61f

                setDrawCenterText(true)

                rotationAngle = 0f
                // enable rotation of the chart by touch
                isRotationEnabled = true
                isHighlightPerTapEnabled = true

                setPieData(chart, data)
                centerText = data.getTotal2()
                animateY(1400, Easing.EaseInOutQuad)
                // pieShoulderChart.spin(2000, 0, 360);

                val l = legend
                l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
                l.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
                l.orientation = Legend.LegendOrientation.VERTICAL
                l.setDrawInside(false)
                l.isEnabled = false
            }


        }

        fun setPieData(chart: PieChart, data: PieData) {

            val entries = ArrayList<PieEntry>()
            entries.add(PieEntry(data.one))//yellow
            entries.add(PieEntry(data.three))//red
            entries.add(PieEntry(data.two))//green


            val dataSet = PieDataSet(entries, "")
            dataSet.sliceSpace = 1f
            dataSet.selectionShift = 5f

            val colors = ArrayList<Int>()
            colors.add(Color.rgb(76, 175, 80))
            colors.add(Color.rgb(220, 185, 46))
            // colors.add(Color.rgb(121, 145, 157))
            colors.add(Color.rgb(217, 0, 0))

            dataSet.colors = colors
            dataSet.selectionShift = 0f


            chart.data = com.github.mikephil.charting.data.PieData(dataSet)


            chart.invalidate()
        }


        @JvmStatic
        @BindingAdapter("setBarChart")
        fun setBarChart(chart: BarChart?, data: BarData?) {

            if (chart == null || data == null)
                return
            chart.run {
                setDrawBarShadow(false)
                setDrawValueAboveBar(true)

                getDescription().setEnabled(false)

                // if more than 60 entries are displayed in the chart, no values will be
                // drawn
                setMaxVisibleValueCount(60)

                // scaling can now only be done on x- and y-axis separately
                // scaling can now only be done on x- and y-axis separately
                setPinchZoom(false)

                setDrawGridBackground(false)
                // chart.setDrawYLabels(false);

                val xAxis: XAxis = getXAxis()
                xAxis.position = XAxis.XAxisPosition.BOTTOM
                xAxis.setDrawGridLines(false)
                xAxis.granularity = 1f // only intervals of 1 day

                xAxis.labelCount = 7
//        xAxis.setValueFormatter(xAxisFormatter as ValueFormatter?)

                val custom: IAxisValueFormatter = MyAxisValueFormatter()

                val leftAxis: YAxis = getAxisLeft()
                leftAxis.setLabelCount(8, false)
//        leftAxis.setValueFormatter(custom as ValueFormatter?)
                leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)
                leftAxis.spaceTop = 15f
                leftAxis.axisMinimum = 0f // this replaces setStartAtZero(true)

                getAxisRight().isEnabled = false

                val l: Legend = getLegend()
                l.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
                l.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
                l.orientation = Legend.LegendOrientation.HORIZONTAL
                l.setDrawInside(false)
                l.form = Legend.LegendForm.SQUARE
                l.formSize = 9f
                l.textSize = 11f
                l.xEntrySpace = 4f
                setBarData(chart,data.xvalues, data.yvalues)
//                val mv = XYMarkerView(activity, xAxisFormatter)
//                mv.setChartView(rootView.chart1) // For bounds control
//                rootView.chart1.setMarker(mv) // Set the marker to the chart
            }
        }

        private fun setBarData(chart: BarChart, count: Float, range: Float) {
            val start = 1f
            val values = java.util.ArrayList<BarEntry>()
            var i: Float = start.toFloat()
            while (i < count) {
                val ss = (Math.random() * (range + 1)).toFloat()
                values.add(BarEntry(i, ss))
//
                i++
            }
            val set1: BarDataSet
            if (chart.getData() != null &&
                chart.getData().getDataSetCount() > 0
            ) {
                set1 = chart.getData().getDataSetByIndex(0) as BarDataSet
                set1.values = values
                chart.getData().notifyDataChanged()
                chart.notifyDataSetChanged()
            } else {
                set1 = BarDataSet(values, "The year 2020")
                set1.setDrawIcons(false)

                val dataSets = java.util.ArrayList<IBarDataSet>()
                dataSets.add(set1)
                val data = com.github.mikephil.charting.data.BarData(dataSets)
                data.setValueTextSize(10f)
                data.barWidth = 0.9f
                chart.setData(data)
            }
        }

        @JvmStatic
        @BindingAdapter("uriImage")
        fun uriImage(view: ImageView?, uriImage: String?) {

            if (view == null || uriImage == null)
                return

            view.setImageURI(Uri.parse(uriImage))

            view.setOnClickListener {
//                ImageUtils.showImageUri(Uri.parse(uriImage), view.context)
            }

        }


        @JvmStatic
        @BindingAdapter("choronmeterSetting")
        fun choronmeterSetting(ch_meter: Chronometer?, timieinmillis: String?) {


            try {
                if (timieinmillis!!.equals("") || timieinmillis == null || ch_meter == null)
                    return
// 60*1000*2  2 mint
                val timeInMil = 60 * 60 * 1000  // 1 hour
                val timeInMilSeconds = System.currentTimeMillis() - timieinmillis.toLong()
//        Log.e("", "setListener: "+System.currentTimeMillis() +" -- "+SystemClock.elapsedRealtime())
//        Log.e("", "timeInMilSeconds: "+timeInMilSeconds)

                ch_meter.base = SystemClock.elapsedRealtime() - timeInMilSeconds
                ch_meter.start()
            } catch (e: Exception) {
            }

        }

        @JvmStatic
        @BindingAdapter("openImage")
        fun openImage(view: ImageView?, image: String?) {
            if (view == null || image == null)
                return

            try {
                Glide.with(view.context)
                    .load(image)
                    .placeholder(R.drawable.defaultimg)
                    .into(view)

                view.setOnClickListener {
                    ImageUtils.showImage(image, view.context)
                }
            } catch (e: Exception) {
            }
        }


        @JvmStatic
        @BindingAdapter("closedImage")
        fun closedImage(view: ImageView?, image: String?) {

            try {
                if (view == null || image == null)
                    return

                Glide.with(view.context)
                    .load(image)
                    .placeholder(R.drawable.defaultimg)
                    .into(view)
            } catch (e: Exception) {
            }
        }


        @JvmStatic
        @BindingAdapter("capitalizeText")
        fun capitalizeText(view: TextView?, str: String?) {

            if (view == null || str == null)
                return


            view.text = ExtraUtils.capitalizeText(str)
            view.setEllipsize(TextUtils.TruncateAt.MARQUEE)
            view.isSelected = true

        }


        fun setDrawableImage(view: RoundedImageViw?, drawableId: Drawable) {
            try {
                Glide.with(view!!.context)
                    .load(drawableId)
                    .placeholder(R.drawable.defaultimg)
                    .into(view)
            } catch (e: Exception) {
            }
        }


        @JvmStatic
        @BindingAdapter("audioRating")
        fun audioRating(textView: TextView, amt: Double?) {
            textView.text = textView.context.getString(
                R.string.audio_rating, ExtraUtils.getFormatedNumber(
                    amt
                ).toString()
            )
        }

        @JvmStatic
        @BindingAdapter("rupee")
        fun rupee(textView: TextView?, rate: Double?) {

            if (textView == null || rate == null)
                return

            textView.text = textView.context.getString(
                R.string.rupee, ExtraUtils.coolFormat(
                    rate, 0
                ).toString()
            )
        }


        fun setIntAndExpert(view: TextView?, expert: Boolean?) {

            if (expert!!)
                view?.text = view?.context?.getString(R.string.expertise)
            else
                view?.text = view?.context?.getString(R.string.interest)

        }

    }


}
