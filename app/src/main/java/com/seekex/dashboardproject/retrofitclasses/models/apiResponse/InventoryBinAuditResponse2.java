
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

public class InventoryBinAuditResponse2 {
    @SerializedName("chart_data")
    private ChartMultisetResponse chart_data;

    public ChartMultisetResponse getChart_data() {
        return chart_data;
    }

    public void setChart_data(ChartMultisetResponse chart_data) {
        this.chart_data = chart_data;
    }





}

