
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

public class InventoryPickPackResponse2 {
    @SerializedName("chart_data")
    private ChartMultisetPickPackResponse chart_data;

    public ChartMultisetPickPackResponse getChart_data() {
        return chart_data;
    }

    public void setChart_data(ChartMultisetPickPackResponse chart_data) {
        this.chart_data = chart_data;
    }





}

