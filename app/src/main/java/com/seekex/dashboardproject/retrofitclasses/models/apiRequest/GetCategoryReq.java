package com.seekex.dashboardproject.retrofitclasses.models.apiRequest;

import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.RequestBase;

public class GetCategoryReq extends RequestBase {

    private GetCategoryReq2 conditions;

    public GetCategoryReq2 getConditions() {
        return conditions;
    }

    public void setConditions(GetCategoryReq2 conditions) {
        this.conditions = conditions;
    }
}
