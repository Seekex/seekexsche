
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

public class OutgoingResponse2 {
    @SerializedName("chart_data")
    private ChartResponse chart_data;

    @SerializedName("next_api")
    private OutgoingNextApiResponse next_api;

    public ChartResponse getChart_data() {
        return chart_data;
    }

    public void setChart_data(ChartResponse chart_data) {
        this.chart_data = chart_data;
    }

    public OutgoingNextApiResponse getNext_api() {
        return next_api;
    }

    public void setNext_api(OutgoingNextApiResponse next_api) {
        this.next_api = next_api;
    }



}

