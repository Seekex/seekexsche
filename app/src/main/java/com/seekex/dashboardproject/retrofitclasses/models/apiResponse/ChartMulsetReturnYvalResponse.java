
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

public class ChartMulsetReturnYvalResponse {
    @SerializedName("carrierReturn")
    private String carrierReturn;
    @SerializedName("customerReturn")
    private String customerReturn;

    public String getCarrierReturn() {
        return carrierReturn;
    }

    public void setCarrierReturn(String carrierReturn) {
        this.carrierReturn = carrierReturn;
    }

    public String getCustomerReturn() {
        return customerReturn;
    }

    public void setCustomerReturn(String customerReturn) {
        this.customerReturn = customerReturn;
    }
}

