package com.seekx.webService.models.apiRequest

data class LogoutReq(
    var uid: Long?,
    var deviceId: String)
