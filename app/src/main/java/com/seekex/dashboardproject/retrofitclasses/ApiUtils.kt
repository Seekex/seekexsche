package com.seekx.webService

import android.content.Context
import android.util.Log
import com.seekex.dashboardproject.AppConstants
import com.seekex.dashboardproject.Preferences
import com.seekx.webService.interfaces.ApiServices

class ApiUtils {

    companion object {
//        const val APIPREFIX = "api"
        const val APIPREFIX = "https://stagging.sevenrocks.in/dashboard-web-api/"
//        const val APIPREFIX = "https://develop-erp.sevenrocks.in/ActivityMasters"
        const val API_HEADER = "Content-Type: application/json;charset=UTF-8"

        const val LOGIN =  "login"
        const val GETBARDATAINVENTORY = "inventory_scanable_out_interval_wise"
        const val GETCATEGORYLIST = "category_list_get"
        const val GETBARDATAINVENTORYINt = "inventory_scanable_in_interval_wise"
        const val GETBARPIEDATA = "inventory_scanable_category_wise"
        const val GETPIEDATATYPEWISE = "inventory_invoice_created_type_wise"
        const val GETPIEDATACOMPLETEWISE = "inventory_invoice_complete_wise"
        const val GETPIEDATATOPINCOMING = "inventory_top_incoming_product"
        const val GETPIEDATATOPUPCOMING = "inventory_top_outgoing_product"
        const val GETBINAUDITDATA = "inventory_bin_audit"
        const val GRADEDATA = "inventory_grade_b_grade_c_movement"
        const val GETBINAUDITCOUNTDATA = "inventory_bin_audit_count"
        const val ORDERRETURNWISE = "order_return_type_wise"
        const val GETBINAUDITDISPATCH = "inventory_pick_pack_dispatch"
        const val GETINVENTORYINNONSCANABLE = "inventory_non_scanable_in_interval_wise"
        const val GETINVENTORYOUTNONSCANABLE = "inventory_non_scanable_out_interval_wise"
        const val GETINVENTORYLOSTMARK = "inventory_scannable_lost_mark"
        const val SUBLOCATIONTRANSACTION = "sub_location_transaction"
        const val CURRENTSNAPSHOT = "current_inventory_snapshot"
        const val OUTGOINGPARTYWISE = "outgoing_in_pc_party_wise"
        const val PARTYOUTGOING = "party_outgoing_category_wise"
        const val OUTGOINGKGWISE = "outgoing_in_kg_party_wise"
        const val ACTIVITYList = "activity_get"
        const val ACTIVITY_Start = "activity_start"
        const val TASK_START = "task_start"
        const val TASK_STOP = "task_stop"
        const val ACTIVITY_Stop = "activity_stop"
        const val TASK_LIST = "task_get"
        const val ACTIVITYTimer = APIPREFIX + "/postActivity"
        const val LOGOUT = APIPREFIX + "/logout"

//        const val localUrl = "http://54.172.167.148:3333/"
        const val localUrl = "https://seekex.in/"


        fun getWebApi(context: Context): ApiServices {
                var pref: Preferences

                pref = Preferences(context)
                Log.e("TAG", "getWebApi: "+pref.get(AppConstants.BASEURL)+"dashboard-web-api/" )


            return RetrofitClient.getClient(pref.get(AppConstants.BASEURL)+"dashboard-web-api/")!!.create(ApiServices::class.java)
        }
        fun getUploadApi(): ApiServices {
            return RetrofitClient.getUploadFileClient(APIPREFIX)!!.create(ApiServices::class.java)
        }

    }

}