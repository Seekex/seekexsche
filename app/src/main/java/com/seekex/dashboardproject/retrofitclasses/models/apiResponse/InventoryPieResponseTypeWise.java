
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class

InventoryPieResponseTypeWise extends ResponseBase {

    @SerializedName("data")
    private ArrayList<InventoryPieResponse.InventoryPieResponse2> data;

    @SerializedName("title")
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<InventoryPieResponse.InventoryPieResponse2> getData() {
        return data;
    }

    public void setData(ArrayList<InventoryPieResponse.InventoryPieResponse2> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Login{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", service_name='" + service_name + '\'' +

                '}';
    }
    public class InventoryPieResponseTypeWise2 {
        @SerializedName("name")
        private String name;

        @SerializedName("value")
        private String value;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}

