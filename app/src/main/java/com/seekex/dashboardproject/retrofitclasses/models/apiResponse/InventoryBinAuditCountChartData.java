
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

public class InventoryBinAuditCountChartData {
    @SerializedName("chart_data")
    private ChartMulsetAUDITResponse chart_data;

    public ChartMulsetAUDITResponse getChart_data() {
        return chart_data;
    }

    public void setChart_data(ChartMulsetAUDITResponse chart_data) {
        this.chart_data = chart_data;
    }





}

