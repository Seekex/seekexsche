package com.seekex.dashboardproject.retrofitclasses.models.apiRequest;

public interface ApiCallBacknew {
    void onSuccess(String serviceName,Object object);
    void onFailed(String msg);
}
