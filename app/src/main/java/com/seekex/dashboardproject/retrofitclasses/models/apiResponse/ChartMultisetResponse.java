
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

public class ChartMultisetResponse {
    @SerializedName("name")
    private String name;
    @SerializedName("xValue")
    private String xValue;
    @SerializedName("yValue")
    private ChartMulsetResponse yValue;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getxValue() {
        return xValue;
    }

    public void setxValue(String xValue) {
        this.xValue = xValue;
    }

    public ChartMulsetResponse getyValue() {
        return yValue;
    }

    public void setyValue(ChartMulsetResponse yValue) {
        this.yValue = yValue;
    }



}

