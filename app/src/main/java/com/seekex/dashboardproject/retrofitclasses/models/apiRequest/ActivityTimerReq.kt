package com.seekx.webService.models.apiRequest

data class ActivityTimerReq(
    var user_id: String,
    var activity_id: String,
    var stop_time: String,
    var stop_time_millis: Long,
    var start_time_millis: Long,
    var start_time: String,
    var status: String,
    var id: String,
){

    constructor():this("","","",0L,0L,"","","")
}
