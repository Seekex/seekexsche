package com.seekex.dashboardproject.retrofitclasses.models.apiRequest;


import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.RequestBase;

public class OutgoingReqData extends RequestBase {

    private OutgoingDTO  conditions;

    public OutgoingDTO getConditions() {
        return conditions;
    }

    public void setConditions(OutgoingDTO conditions) {
        this.conditions = conditions;
    }
}
