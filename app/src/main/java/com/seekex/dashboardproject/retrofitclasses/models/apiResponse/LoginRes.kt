package com.seekx.webService.models.apiResponse

import com.seekx.webService.models.BaseResponse

data class LoginRes(val user_id: Long?, val group_id:String, val dept_id:String, val firstname:String, val lastname:String, val mobile_no:String):BaseResponse(0,"")

