package com.seekex.dashboardproject.retrofitclasses.models.apiResponse

import com.seekex.dashboardproject.retrofitclasses.models.ActivityDTO
import com.seekx.webService.models.BaseResponse

data class ActivityRes(val activityData: ArrayList<ActivityDTO>,val is_status:String): BaseResponse(0,"")
