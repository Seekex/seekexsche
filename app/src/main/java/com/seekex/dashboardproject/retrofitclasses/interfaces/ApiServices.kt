package com.seekx.webService.interfaces

import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.GetCategoryReq
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.OutgoingReqData
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.SearchInvenReqData
import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.*
import com.seekx.webService.models.apiRequest.*
import retrofit2.http.*
import rx.Observable


interface ApiServices {

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getTasks(@Body `object`: RequestBase?): Observable<TaskResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getBarData(@Body `object`: SearchInvenReqData?): Observable<InventoryResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getCategoryList(@Body `object`: GetCategoryReq?): Observable<GetCategoryResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getBarAuditData(@Body `object`: SearchInvenReqData?): Observable<InventoryBinAuditResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getGradedata(@Body `object`: SearchInvenReqData?): Observable<GradeResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getBarAuditCountData(@Body `object`: SearchInvenReqData?): Observable<InventoryBinAuditCountResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getReturnwise(@Body `object`: SearchInvenReqData?): Observable<ReturnwiseFragmentResponse>?


    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getPickPackData(@Body `object`: SearchInvenReqData?): Observable<InventoryPickPackResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun login(@Body loginReq: LoginReq): Observable<LoginResponse>


    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun updateTimer(@Body loginReq: ActivitystartReq): Observable<ResponseBase>

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun updateTaskTimer(@Body loginReq: TaskstartReq): Observable<ResponseBase>

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun stopTaskTimer(@Body loginReq: Taskstopreq): Observable<ResponseBase>

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getpiedata(@Body loginReq: SearchInvenReqData): Observable<InventoryPieResponse>


    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getpiedOutgoingdata(@Body loginReq: OutgoingReqData): Observable<InventoryPieResponse>


    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getpiedataTypeWise(@Body loginReq: SearchInvenReqData): Observable<InventoryPieResponseTypeWise>
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getOutgoingData(@Body `object`: SearchInvenReqData?): Observable<OutgoingResponse>?

}