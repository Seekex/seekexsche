
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

public class OutgoingNextApiResponse {
    @SerializedName("sub_chart_title")
    private String sub_chart_title;

    @SerializedName("api_name")
    private String api_name;

    @SerializedName("conditions")
    private OutgoingNextApiCondResponse conditions;

    public String getApi_name() {
        return api_name;
    }

    public void setApi_name(String api_name) {
        this.api_name = api_name;
    }

    public String getSub_chart_title() {
        return sub_chart_title;
    }

    public void setSub_chart_title(String sub_chart_title) {
        this.sub_chart_title = sub_chart_title;
    }

    public OutgoingNextApiCondResponse getConditions() {
        return conditions;
    }

    public void setConditions(OutgoingNextApiCondResponse conditions) {
        this.conditions = conditions;
    }

}

