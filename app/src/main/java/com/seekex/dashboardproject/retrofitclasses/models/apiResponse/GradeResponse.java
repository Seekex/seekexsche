
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GradeResponse extends ResponseBase {

    @SerializedName("data")
    private ArrayList<GradeResponse2> data;

    @SerializedName("title")
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<GradeResponse2> getData() {
        return data;
    }

    public void setData(ArrayList<GradeResponse2> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Login{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", service_name='" + service_name + '\'' +

                '}';
    }
    public class GradeResponse2 {
        @SerializedName("chart_data")
        private GradeMultisetResponse chart_data;

        public GradeMultisetResponse getChart_data() {
            return chart_data;
        }

        public void setChart_data(GradeMultisetResponse chart_data) {
            this.chart_data = chart_data;
        }


    }

    public class GradeMultisetResponse {
        @SerializedName("id")
        private String id;

        @SerializedName("name")
        private String name;
        @SerializedName("xValue")
        private String xValue;
        @SerializedName("yValue")
        private GradeYResponse yValue;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getxValue() {
            return xValue;
        }

        public void setxValue(String xValue) {
            this.xValue = xValue;
        }

        public GradeYResponse getyValue() {
            return yValue;
        }

        public void setyValue(GradeYResponse yValue) {
            this.yValue = yValue;
        }



    }
    public class GradeYResponse {
        @SerializedName("grade_b")
        private String grade_b;
        @SerializedName("grade_c")
        private String grade_c;

        public String getGrade_b() {
            return grade_b;
        }

        public void setGrade_b(String grade_b) {
            this.grade_b = grade_b;
        }

        public String getGrade_c() {
            return grade_c;
        }

        public void setGrade_c(String grade_c) {
            this.grade_c = grade_c;
        }
    }

}

