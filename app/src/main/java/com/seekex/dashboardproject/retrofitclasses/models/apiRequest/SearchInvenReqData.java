package com.seekex.dashboardproject.retrofitclasses.models.apiRequest;


import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.RequestBase;

public class SearchInvenReqData extends RequestBase {

    private SearchInvenDTO  conditions;

    public SearchInvenDTO getConditions() {
        return conditions;
    }

    public void setConditions(SearchInvenDTO conditions) {
        this.conditions = conditions;
    }
}
