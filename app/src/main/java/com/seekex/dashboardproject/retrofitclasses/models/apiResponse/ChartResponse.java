
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ChartResponse {
    @SerializedName("name")
    private String name;
    @SerializedName("xValue")
    private String xValue;
    @SerializedName("yValue")
    private String yValue;
    @SerializedName("id")
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getxValue() {
        return xValue;
    }

    public void setxValue(String xValue) {
        this.xValue = xValue;
    }

    public String getyValue() {
        return yValue;
    }

    public void setyValue(String yValue) {
        this.yValue = yValue;
    }



}

