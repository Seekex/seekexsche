package com.seekex.dashboardproject.retrofitclasses.models


class ActivityDTO(
    var activity_id: String,
    var id: String,
    val activity_name: String,
    val duration: String,
    var start_time: String,
    var stop_time: String,
    var user_id: String,
    var isStarted: String,
    var start_time_millis: String
) {
    fun ShowTimer(): Boolean {
        if (isStarted.equals("Yes")) {
            return true
        } else {
            return false
        }

    }

    constructor() : this("","", "", "", "", "", "", "", "")
}

