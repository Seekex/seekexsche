package com.seekx.webService.models.apiRequest

import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.RequestBase

data class LoginReq(
    var gcm_reg_no: String,
    var otp: String):RequestBase(){

    constructor():this("","")
}
