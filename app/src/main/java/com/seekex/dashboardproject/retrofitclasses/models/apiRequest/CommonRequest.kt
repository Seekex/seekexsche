package com.seekx.webService.models.apiRequest

import com.seekx.webService.models.BaseRequest

data class CommonRequest(var keyValue: String,
                         var id: Long,
                         var expertId: Long?,
                         var type: Long,
                         var page: Int?=null):BaseRequest(){

}
