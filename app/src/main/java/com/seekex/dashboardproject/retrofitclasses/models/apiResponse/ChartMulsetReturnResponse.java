
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

public class ChartMulsetReturnResponse {
    @SerializedName("xValue")
    private String xValue;
    @SerializedName("yValue")
    private ChartMulsetReturnYvalResponse yValue;

    public String getxValue() {
        return xValue;
    }

    public void setxValue(String xValue) {
        this.xValue = xValue;
    }

    public ChartMulsetReturnYvalResponse getyValue() {
        return yValue;
    }

    public void setyValue(ChartMulsetReturnYvalResponse yValue) {
        this.yValue = yValue;
    }
}

