
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

public class ChartMultisetPickPackResponse {
    @SerializedName("name")
    private String name;
    @SerializedName("xValue")
    private String xValue;
    @SerializedName("id")
    private String id;
    @SerializedName("yValue")
    private ChartMulsetPickPackResponse yValue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getxValue() {
        return xValue;
    }

    public void setxValue(String xValue) {
        this.xValue = xValue;
    }

    public ChartMulsetPickPackResponse getyValue() {
        return yValue;
    }

    public void setyValue(ChartMulsetPickPackResponse yValue) {
        this.yValue = yValue;
    }



}

