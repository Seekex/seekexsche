package com.seekx.webService

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.google.gson.Gson
import com.seekex.dashboardproject.AppConstants
import com.seekex.dashboardproject.Preferences
import com.seekex.dashboardproject.R
import com.seekex.dashboardproject.custombinding.CustomLoader
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.SearchInvenReqData
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.ApiCallBacknew
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.GetCategoryReq
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.OutgoingReqData
import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.*
import com.seekx.utils.DialogUtils
import com.seekx.utils.ExtraUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.seekx.webService.models.BaseResponse
import com.seekx.webService.models.apiRequest.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


open class ApiImp(val context: Context) {

    var pref: Preferences = Preferences(context)
    var dialogUtil: DialogUtils = DialogUtils(context)


    companion object {
        const val TAG = "ApiImpResp"
    }


    private val webApi = ApiUtils.getWebApi(context)
    private var apiCallBacknew: ApiCallBacknew? = null

    var customLoader: CustomLoader = CustomLoader(context)
    private val uploadFileApi = ApiUtils.getUploadApi()

    fun showProgressBar() {
        customLoader.show()
    }

    fun cancelProgressBar() {
        customLoader.cancel()
    }

    fun setApiCallBacknew(apiCallBacknew: ApiCallBacknew?) {
        this.apiCallBacknew = apiCallBacknew
    }

    fun login(loginReq: LoginReq) {

        if (!onStartApi())
            return
        Log.e("TAG", "loginrequest: " + Gson().toJson(loginReq))
        webApi.login(loginReq).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<LoginResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError login: " + e.toString())
                    onResponseApi(e.toString(), null)
                }

                @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
                @SuppressLint("LongLogTag")
                override fun onNext(addUserRes: LoginResponse) {
                    Log.e(TAG, "onlogin: " + addUserRes.toString())
                    Log.e(ApiUtils.LOGIN, addUserRes.toString())
                    if (addUserRes.getStatus() == 0) apiCallBacknew!!.onFailed(addUserRes.getMsg()) else {
                        apiCallBacknew!!.onSuccess(addUserRes.getService_name(), addUserRes)
                    }
                    onResponseApi(addUserRes.getStatus() == 0, addUserRes.getMsg())

                }

            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun gettasks(activityReq: RequestBase) {
        if (!onStartApi(activityReq)) return

        Log.e("TAG", "activityRequest: " + Gson().toJson(activityReq))
        webApi.getTasks(activityReq)?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(object : Observer<TaskResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError activity: " + e.toString())
                    onResponseApi(
                        true, """
     ${activityReq.getService_name()}
     $e
     """.trimIndent()
                    )
                              }

                @SuppressLint("LongLogTag")
                override fun onNext(respnse: TaskResponse) {
                    Log.e(TAG, "get activity: " + respnse.toString())
                    Log.v(ApiUtils.LOGIN, respnse.toString())
                    if (respnse.getStatus() == 0) apiCallBacknew!!.onFailed(respnse.getMsg()) else {
                        apiCallBacknew!!.onSuccess(respnse.getService_name(), respnse)
                    }
                    onResponseApi(respnse.getStatus() == 0, respnse.getMsg())


                }

            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getPickPackData(activityReq: SearchInvenReqData) {
        if (!onStartApi(activityReq)) return

        Log.e("TAG", "activityRequest: " + Gson().toJson(activityReq))
        webApi.getPickPackData(activityReq)?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(object : Observer<InventoryPickPackResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError activity: " + e.toString())
//                    apiCallBacknew!!.onFailed(e.toString())
                    onResponseApi(
                        true, """
     ${activityReq.getService_name()}
     $e
     """.trimIndent()
                    )
                }

                @SuppressLint("LongLogTag")
                override fun onNext(respnse: InventoryPickPackResponse) {
                    Log.e(TAG, "get activity: " + respnse.toString())
                    Log.v(ApiUtils.LOGIN, respnse.toString())
                    if (respnse.getStatus() == 0) apiCallBacknew!!.onFailed(respnse.getMsg()) else {
                        apiCallBacknew!!.onSuccess(respnse.getService_name(), respnse)
                    }
                    onResponseApi(respnse.getStatus() == 0, respnse.getMsg())


                }

            })
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getreturnwise(activityReq: SearchInvenReqData, param: ApiCallBack) {
        if (!onStartApi(activityReq)) return

        Log.e("TAG", "activityRequest: " + Gson().toJson(activityReq))
        webApi.getReturnwise(activityReq)?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(object : Observer<ReturnwiseFragmentResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError activity: " + e.toString())
//                    apiCallBacknew!!.onFailed(e.toString())
                    onResponseApi(
                        true, """
     ${activityReq.getService_name()}
     $e
     """.trimIndent()
                    )
                }

                @SuppressLint("LongLogTag")
                override fun onNext(respnse: ReturnwiseFragmentResponse) {
                    Log.e(TAG, "get activity: " + respnse.toString())
                    Log.v(ApiUtils.LOGIN, respnse.toString())
                    if (respnse.getStatus() == 0) apiCallBacknew!!.onFailed(respnse.getMsg()) else {
                        apiCallBacknew!!.onSuccess(respnse.getService_name(), respnse)
                    }
                    onResponseApi(respnse,param)


                }

            })
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getBarBinAuditCountData(activityReq: SearchInvenReqData, param: ApiCallBack) {
        if (!onStartApi(activityReq)) return

        Log.e("TAG", "activityRequest: " + Gson().toJson(activityReq))
        webApi.getBarAuditCountData(activityReq)?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(object : Observer<InventoryBinAuditCountResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError activity: " + e.toString())
//                    apiCallBacknew!!.onFailed(e.toString())
                    onResponseApi(
                        true, """
     ${activityReq.getService_name()}
     $e
     """.trimIndent()
                    )
                }

                @SuppressLint("LongLogTag")
                override fun onNext(respnse: InventoryBinAuditCountResponse) {
                    Log.e(TAG, "get activity: " + respnse.toString())
//                    if (respnse.getStatus() == 0) apiCallBacknew!!.onFailed(respnse.getMsg()) else {
//                        apiCallBacknew!!.onSuccess(respnse.getService_name(), respnse)
//                    }
                    onResponseApi(respnse, param)

                }

            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getBarGradeData(activityReq: SearchInvenReqData, param: ApiCallBack) {
        if (!onStartApi(activityReq)) return

        Log.e("TAG", "activityRequest: " + Gson().toJson(activityReq))
        webApi.getGradedata(activityReq)?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(object : Observer<GradeResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError activity: " + e.toString())
//                    apiCallBacknew!!.onFailed(e.toString())
                    onResponseApi(
                        true, """
     ${activityReq.getService_name()}
     $e
     """.trimIndent()
                    )
                }

                @SuppressLint("LongLogTag")
                override fun onNext(respnse: GradeResponse) {
                    Log.e(TAG, "get activity: " + respnse.toString())
                    Log.v(ApiUtils.LOGIN, respnse.toString())
//                    if (respnse.getStatus() == 0) apiCallBacknew!!.onFailed(respnse.getMsg()) else {
//                        apiCallBacknew!!.onSuccess(respnse.getService_name(), respnse)
//                    }
                    onResponseApi(respnse,param)


                }

            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getBarAuditData(activityReq: SearchInvenReqData, param: ApiCallBack) {
        if (!onStartApi(activityReq)) return

        Log.e("TAG", "activityRequest: " + Gson().toJson(activityReq))
        webApi.getBarAuditData(activityReq)?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(object : Observer<InventoryBinAuditResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError activity: " + e.toString())
//                    apiCallBacknew!!.onFailed(e.toString())
                    onResponseApi(
                        true, """
     ${activityReq.getService_name()}
     $e
     """.trimIndent()
                    )
                }

                @SuppressLint("LongLogTag")
                override fun onNext(respnse: InventoryBinAuditResponse) {
                    Log.e(TAG, "get activity: " + respnse.toString())
                    Log.v(ApiUtils.LOGIN, respnse.toString())
                    if (respnse.getStatus() == 0) apiCallBacknew!!.onFailed(respnse.getMsg()) else {
                        apiCallBacknew!!.onSuccess(respnse.getService_name(), respnse)
                    }
                    onResponseApi(respnse,param)


                }

            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getCategoryList(activityReq: GetCategoryReq, apiCallBack: ApiCallBack) {
        if (!onStartApi(activityReq)) return

        Log.e("TAG", "activityRequest: " + Gson().toJson(activityReq))
        webApi.getCategoryList(activityReq)?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(object : Observer<GetCategoryResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError activity: " + e.toString())
                    onResponseApi(e.toString(), null)
                }

                @SuppressLint("LongLogTag")
                override fun onNext(respnse: GetCategoryResponse) {
                    Log.e(TAG, "get activity: " + respnse.toString())
//                    if (respnse.getStatus() == 0) apiCallBacknew!!.onFailed(respnse.getMsg()) else {
//                        apiCallBacknew!!.onSuccess(respnse.getService_name(), respnse)
//                    }
//                    onResponseApi(respnse.getStatus() == 0, respnse.getMsg())
                    onResponseApi(respnse, apiCallBack)

                }

            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getOutgoingData(activityReq: SearchInvenReqData) {
        if (!onStartApi(activityReq)) return

        Log.e("TAG", "activityRequest: " + Gson().toJson(activityReq))
        webApi.getOutgoingData(activityReq)?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(object : Observer<OutgoingResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError activity: " + e.toString())
//                    onResponseApi(e.toString(), null)
                }

                @SuppressLint("LongLogTag")
                override fun onNext(respnse: OutgoingResponse) {
                    Log.e(TAG, "get activity: " + respnse.toString())
                    if (respnse.getStatus() == 0) apiCallBacknew!!.onFailed(respnse.getMsg()) else {
                        apiCallBacknew!!.onSuccess(respnse.getService_name(), respnse)
                    }
                    onResponseApi(respnse.getStatus() == 0, respnse.getMsg())


                }

            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getBarData(activityReq: SearchInvenReqData, param: ApiCallBack) {
        if (!onStartApi(activityReq)) return

        Log.e("TAG", "activityRequest: " + Gson().toJson(activityReq))
        webApi.getBarData(activityReq)?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(object : Observer<InventoryResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError activity: " + e.toString())
                    onResponseApi(e.toString(), null)
                }

                @SuppressLint("LongLogTag")
                override fun onNext(respnse: InventoryResponse) {
                    Log.e(TAG, "get activity: " + respnse.toString())
//                    if (respnse.getStatus() == 0) apiCallBacknew!!.onFailed(respnse.getMsg()) else {
//                        apiCallBacknew!!.onSuccess(respnse.getService_name(), respnse)
//                    }
//                    onResponseApi(respnse.getStatus() == 0, respnse.getMsg())

                    onResponseApi(respnse, param)
                }

            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getPiedataTypewise(activityReq: SearchInvenReqData) {

        if (!onStartApinew(activityReq)) return
        Log.e("TAG", "activity timeRequest: " + Gson().toJson(activityReq))
        webApi.getpiedataTypeWise(activityReq).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<InventoryPieResponseTypeWise> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError activity: " + e.toString())

                    onResponseApi(
                        true, """
     ${activityReq.getService_name()}
     $e
     """.trimIndent()
                    )
                }

                @SuppressLint("LongLogTag")
                override fun onNext(respnse: InventoryPieResponseTypeWise) {
                    Log.e(TAG, "get timer: " + respnse.toString())
                    if (respnse.getStatus() == 0) onResponseApi(
                        respnse.getStatus() == 0,
                        respnse.getMsg()
                    ) else {
                        apiCallBacknew!!.onSuccess(respnse.getService_name(), respnse)
                    }

                }

            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getOutgoingPiedata(activityReq: OutgoingReqData) {

        if (!onStartApinew(activityReq)) return
        Log.e("TAG", "activity timeRequest: " + Gson().toJson(activityReq))
        webApi.getpiedOutgoingdata(activityReq).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<InventoryPieResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError activity: " + e.toString())

                    onResponseApi(
                        true, """
     ${activityReq.getService_name()}
     $e
     """.trimIndent()
                    )
                }

                @SuppressLint("LongLogTag")
                override fun onNext(respnse: InventoryPieResponse) {
                    Log.e(TAG, "get timer: " + respnse.toString())
                    if (respnse.getStatus() == 0) onResponseApi(
                        respnse.getStatus() == 0,
                        respnse.getMsg()
                    ) else {
                        apiCallBacknew!!.onSuccess(respnse.getService_name(), respnse)
                    }

                }

            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getPiedata(activityReq: SearchInvenReqData) {

        if (!onStartApinew(activityReq)) return
        Log.e("TAG", "activity timeRequest: " + Gson().toJson(activityReq))
        webApi.getpiedata(activityReq).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<ResponseBase> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError activity: " + e.toString())

                    onResponseApi(
                        true, """
     ${activityReq.getService_name()}
     $e
     """.trimIndent()
                    )
                }

                @SuppressLint("LongLogTag")
                override fun onNext(respnse: ResponseBase) {
                    Log.e(TAG, "get timer: " + respnse.toString())
                    if (respnse.getStatus() == 0) onResponseApi(
                        respnse.getStatus() == 0,
                        respnse.getMsg()
                    ) else {
                        apiCallBacknew!!.onSuccess(respnse.getService_name(), respnse)
                    }

                }

            })
    }



    private fun checkInternet(): Boolean {
        if (!ExtraUtils.isNetworkConnectedMainThread(context)) {
            dialogUtil.showAlert(context.getString(R.string.internet_error))
            return false
        }
        return true
    }

    private fun onStartApi(): Boolean {
        if (!checkInternet()) {
            dialogUtil.showAlert(context.getString(R.string.internet_error))
            return false
        }
//        customLoader.show()
        return true
    }


    private fun checkResMsg(msg: String): String {
        if (msg.contains("java.net") || msg.contains("retrofit2.adapter.rxjava")) {
//            toast(Preferences(context).get(AppConstants.token))
            return context.getString(R.string.connection_failed)
        }

        return msg
    }

    private fun onResponseApi(response: BaseResponse, Obj: Any?, apiCallBack: ApiCallBack) {
        customLoader.cancel()
        if (response.status == 0) {

            dialogUtil.showAlert(checkResMsg(response.msg))
            apiCallBack.onSuccess(false, response.msg)
        } else {
            apiCallBack.onSuccess(true, Obj!!)
        }
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun onResponseApi(status: Boolean, msg: String?) {
        if (status) {
            dialogUtil.showAlert(msg!!)
        }
        if (!(context as Activity?)!!.isDestroyed) customLoader.cancel()
    }

    private fun onResponseApi(response: ResponseBase, apiCallBack: ApiCallBack?) {
        customLoader.cancel()
        if (response.status == 0) {

//            if (onLogoutCheck(response))
//                return

            dialogUtil.showAlert(checkResMsg(response.msg))
        }
        if (response.status == 0) {
            apiCallBack?.onSuccess(false, response)

        } else {
            apiCallBack?.onSuccess(true, response)

        }

    }

    private fun onResponseApi(msg: String, apiCallBack: ApiCallBack?) {
        customLoader.cancel()
        Log.e("upload 44", "onResponseApi:msg= " + msg)

        dialogUtil.showAlert(checkResMsg(msg))

        apiCallBack?.onSuccess(false, msg)

    }

    open fun isNetworkConnectedMainThred(ctx: Context): Boolean {
        val cm = ctx
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val ni = cm.activeNetworkInfo
        return if (ni == null) {
//            DialogUtil.showInternetError(ctx)
            false
        } else true
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun onStartApi(`object`: RequestBase): Boolean {
        if (!isNetworkConnectedMainThred(context)) {
            dialogUtil.showAlert("No Internet Connection")
            return false
        }
//        `object`.setUsername(pref.get(AppConstants.user_name))
//        `object`.setPassword(pref.get(AppConstants.password))
//        `object`.app_version = context!!.getString(R.string.version)
//        `object`.secret_key = ImageUtils.getSecretKey()
//        `object`.token = pref.get(AppConstants.token)
        if (!(context as Activity?)!!.isDestroyed) customLoader.show()
        return true
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun onStartApinew(`object`: RequestBase): Boolean {
        if (!isNetworkConnectedMainThred(context)) {
            dialogUtil.showAlert("No Internet Connection")
            return false
        }
//        `object`.setUsername(pref.get(AppConstants.user_name))
//        `object`.setPassword(pref.get(AppConstants.password))
        `object`.app_version = context!!.getString(R.string.version)
        `object`.token = pref.get(AppConstants.token)
        if (!(context as Activity?)!!.isDestroyed) customLoader.show()
        return true
    }

}