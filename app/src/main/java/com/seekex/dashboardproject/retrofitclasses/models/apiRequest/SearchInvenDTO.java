package com.seekex.dashboardproject.retrofitclasses.models.apiRequest;


import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.RequestBase;

public class SearchInvenDTO {

    private String  from_date;
    private String  to_date;
    private String  interval;
    private String  type;

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    private String  category_id;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }
}
