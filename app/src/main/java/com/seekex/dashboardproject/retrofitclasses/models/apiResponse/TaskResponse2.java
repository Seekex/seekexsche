
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

public class TaskResponse2 extends ResponseBase {
    @SerializedName("task_id")
    private String task_id;
    @SerializedName("title")
    private String title;
    @SerializedName("is_started")
    private boolean is_started;
    @SerializedName("task_activity_id")
    private String task_activity_id;
    @SerializedName("start_time_millis")
    private String start_time_millis;
    @SerializedName("start_date")
    private String start_date;
    @SerializedName("start_time")
    private String start_time;

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isIs_started() {
        return is_started;
    }

    public void setIs_started(boolean is_started) {
        this.is_started = is_started;
    }

    public String getTask_activity_id() {
        return task_activity_id;
    }

    public void setTask_activity_id(String task_activity_id) {
        this.task_activity_id = task_activity_id;
    }

    public String getStart_time_millis() {
        return start_time_millis;
    }

    public void setStart_time_millis(String start_time_millis) {
        this.start_time_millis = start_time_millis;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    @Override
    public String toString() {
        return "Login{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", service_name='" + service_name + '\'' +

                '}';
    }

}

