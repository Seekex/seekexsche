
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

public class ChartMulsetAUDITResponse {
    @SerializedName("xValue")
    private String xValue;
    @SerializedName("id")
    private String id;
    @SerializedName("yValue")
    private ChartMulsetAuditYvalResponse yValue;

    public String getxValue() {
        return xValue;
    }

    public void setxValue(String xValue) {
        this.xValue = xValue;
    }

    public ChartMulsetAuditYvalResponse getyValue() {
        return yValue;
    }

    public void setyValue(ChartMulsetAuditYvalResponse yValue) {
        this.yValue = yValue;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public class ChartMulsetAuditYvalResponse {
        @SerializedName("present")
        private String present;
        @SerializedName("lost")
        private String lost;

        public String getPresent() {
            return present;
        }

        public void setPresent(String present) {
            this.present = present;
        }

        public String getLost() {
            return lost;
        }

        public void setLost(String lost) {
            this.lost = lost;
        }
    }

}

