
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

public class GetCategoryResponse2 {
    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

