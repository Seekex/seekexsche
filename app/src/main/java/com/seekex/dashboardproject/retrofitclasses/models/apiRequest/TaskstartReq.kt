package com.seekx.webService.models.apiRequest

import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.RequestBase

data class TaskstartReq(
    var task_id: String,var start_time_millis: Long):RequestBase(){

    constructor():this("",0L)
}
