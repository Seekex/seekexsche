
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

public class ReturnwiseResponse2 {
    @SerializedName("chart_data")
    private ChartMulsetReturnResponse chart_data;

    public ChartMulsetReturnResponse getChart_data() {
        return chart_data;
    }

    public void setChart_data(ChartMulsetReturnResponse chart_data) {
        this.chart_data = chart_data;
    }





}

