
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class InventoryBinAuditResponse extends ResponseBase {

    @SerializedName("data")
    private ArrayList<InventoryBinAuditResponse2> data;

    @SerializedName("title")
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<InventoryBinAuditResponse2> getData() {
        return data;
    }

    public void setData(ArrayList<InventoryBinAuditResponse2> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Login{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", service_name='" + service_name + '\'' +

                '}';
    }

}

