
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

public class InventoryNextApiCondResponse extends ResponseBase {
    @SerializedName("from_date")
    private String from_date;

    @SerializedName("to_date")
    private String to_date;

    @SerializedName("type")
    private String type;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    @Override
    public String toString() {
        return "Login{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", service_name='" + service_name + '\'' +

                '}';
    }

}

