
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TaskResponse extends ResponseBase {
    @SerializedName("data")
    private ArrayList<TaskResponse2> data;



    @SerializedName("is_any_task_start")
    private Boolean is_any_task_start;

    public Boolean getIs_any_task_start() {
        return is_any_task_start;
    }

    public void setIs_any_task_start(Boolean is_any_task_start) {
        this.is_any_task_start = is_any_task_start;
    }

    public ArrayList<TaskResponse2> getData() {
        return data;
    }

    public void setData(ArrayList<TaskResponse2> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Login{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", service_name='" + service_name + '\'' +

                '}';
    }

}

