
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class InventoryNextApiResponse {
    @SerializedName("sub_chart_title")
    private String sub_chart_title;

    @SerializedName("conditions")
    private InventoryNextApiCondResponse conditions;

    public String getSub_chart_title() {
        return sub_chart_title;
    }

    public void setSub_chart_title(String sub_chart_title) {
        this.sub_chart_title = sub_chart_title;
    }

    public InventoryNextApiCondResponse getConditions() {
        return conditions;
    }

    public void setConditions(InventoryNextApiCondResponse conditions) {
        this.conditions = conditions;
    }

}

