package com.seekx.webService.models.apiRequest

import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.RequestBase

data class Activitystopreq(
    var activity_id: String):RequestBase(){

    constructor():this("")
}
