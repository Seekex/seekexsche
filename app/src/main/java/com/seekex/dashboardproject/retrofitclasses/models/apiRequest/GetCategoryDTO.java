package com.seekex.dashboardproject.retrofitclasses.models.apiRequest;


public class GetCategoryDTO {

    private String  is_finish_good;

    public String getIs_finish_good() {
        return is_finish_good;
    }

    public void setIs_finish_good(String is_finish_good) {
        this.is_finish_good = is_finish_good;
    }
}
