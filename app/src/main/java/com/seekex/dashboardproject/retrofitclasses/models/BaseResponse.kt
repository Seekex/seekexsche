package com.seekx.webService.models


open  class BaseResponse(var status: Int, var msg:String){
    override fun toString(): String {
        return "BaseResponse(type=$status, resMsg='$msg')"
    }
}