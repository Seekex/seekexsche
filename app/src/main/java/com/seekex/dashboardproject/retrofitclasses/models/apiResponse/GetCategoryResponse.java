
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetCategoryResponse extends ResponseBase {

    @SerializedName("data")
    private ArrayList<GetCategoryResponse2> data;


    public ArrayList<GetCategoryResponse2> getData() {
        return data;
    }

    public void setData(ArrayList<GetCategoryResponse2> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Login{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", service_name='" + service_name + '\'' +

                '}';
    }

}

