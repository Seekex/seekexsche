
package com.seekex.dashboardproject.retrofitclasses.models.apiResponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class InventoryBinAuditCountResponse extends ResponseBase {

    @SerializedName("data")
    private ArrayList<InventoryBinAuditCountResponse2> data;

    @SerializedName("title")
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<InventoryBinAuditCountResponse2> getData() {
        return data;
    }

    public void setData(ArrayList<InventoryBinAuditCountResponse2> data) {
        this.data = data;
    }
    public class InventoryBinAuditCountResponse2 {
        @SerializedName("chart_data")
        private ChartMulsetAUDITResponse chart_data;

        public ChartMulsetAUDITResponse getChart_data() {
            return chart_data;
        }

        public void setChart_data(ChartMulsetAUDITResponse chart_data) {
            this.chart_data = chart_data;
        }





    }
    @Override
    public String toString() {
        return "Login{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", service_name='" + service_name + '\'' +

                '}';
    }

}

