package com.seekex.dashboardproject.fragments.invnetory

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.formatter.LargeValueFormatter
import com.seekex.dashboardproject.AppConstants
import com.seekex.dashboardproject.R
import com.seekex.dashboardproject.activities.InventoryActivity
import com.seekex.dashboardproject.custom.MyMarkerView
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.ApiCallBacknew
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.SearchInvenDTO
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.SearchInvenReqData
import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.InventoryBinAuditResponse
import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.InventoryBinAuditResponse2
import com.seekx.utils.ImageUtils
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import kotlinx.android.synthetic.main.homeactivity.view.*
import java.util.*

class BarINventoryAuditFragment : Fragment() {

    var mainActivity: InventoryActivity? = null
    private lateinit var rootView: View

    companion object {


        @JvmStatic
        val instance: Fragment
            get() = BarINventoryAuditFragment()
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater!!.inflate(R.layout.homeactivity, container, false)

        mainActivity = activity as InventoryActivity?
        getAllapi()
        clickListener(rootView)
        return rootView
    }

    private fun setdummyData(
        datalist: ArrayList<InventoryBinAuditResponse2>,
        xaxislist: ArrayList<String>
    ) {
        rootView.chart1.getDescription().setEnabled(false)

        rootView.chart1.setPinchZoom(false)

        rootView.chart1.setDrawBarShadow(false)

        rootView.chart1.setDrawGridBackground(false)

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        val mv = MyMarkerView(activity, R.layout.custom_marker_view)
        mv.setChartView(rootView.chart1) // For bounds control

        rootView.chart1.setMarker(mv) // Set the marker to the chart


        val l: Legend = rootView.chart1.getLegend()
        l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
        l.orientation = Legend.LegendOrientation.VERTICAL
        l.setDrawInside(true)
        l.yOffset = 0f
        l.xOffset = 10f
        l.yEntrySpace = 0f
        l.textSize = 8f

        val xAxis: XAxis = rootView.chart1.getXAxis()
        xAxis.granularity = 1f
        xAxis.setCenterAxisLabels(true)
        xAxis.position = XAxis.XAxisPosition.BOTTOM

        val leftAxis: YAxis = rootView.chart1.getAxisLeft()
        leftAxis.valueFormatter = LargeValueFormatter()
        leftAxis.setDrawGridLines(false)
        leftAxis.spaceTop = 35f
        leftAxis.axisMinimum = 0f // this replaces setStartAtZero(true)


        rootView.chart1.getAxisRight().setEnabled(false)
//        setdata(datalist)
        val groupSpace = 0.31f
        val barSpace = 0.03f // x4 DataSet

        val barWidth = 0.2f // x4 DataSet

        // (0.2 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group"
        //
        //(barwidth + barspace) * numBar +
        //0.69
        val groupCount: Int = 3
        val startYear = 1980
        val endYear = 1985


        val values1 = ArrayList<BarEntry>()
        val values2 = ArrayList<BarEntry>()
        val values3 = ArrayList<BarEntry>()
        var i: Int = 0
//        for (i in startYear until endYear) {
        for (item in datalist) {
            values1.add(BarEntry(i.toFloat(), item.chart_data.getyValue().fail.toFloat()))
            values2.add(BarEntry(i.toFloat(), item.chart_data.getyValue().pass.toFloat()))
            values3.add(BarEntry(i.toFloat(), item.chart_data.getyValue().total.toFloat()))
            i++
        }
        xAxis.setValueFormatter(IndexAxisValueFormatter(xaxislist));

        val set1: BarDataSet
        val set2: BarDataSet
        val set3: BarDataSet

        if (rootView.chart1.getData() != null && rootView.chart1.getData().getDataSetCount() > 0) {
            set1 = rootView.chart1.getData().getDataSetByIndex(0) as BarDataSet
            set2 = rootView.chart1.getData().getDataSetByIndex(1) as BarDataSet
            set3 = rootView.chart1.getData().getDataSetByIndex(2) as BarDataSet
            set1.values = values1
            set2.values = values2
            set3.values = values3
            rootView.chart1.getData().notifyDataChanged()
            rootView.chart1.notifyDataSetChanged()
        } else {
            // create 4 DataSets
            set1 = BarDataSet(values1, "Fail")
            set1.color = Color.rgb(104, 241, 175)
            set2 = BarDataSet(values2, "Pass")
            set2.color = Color.rgb(164, 228, 251)
            set3 = BarDataSet(values3, "Total")
            set3.color = Color.rgb(242, 247, 158)

            val data = BarData(set1, set2, set3)
            data.setValueFormatter(LargeValueFormatter())
            rootView.chart1.setData(data)
        }

        // specify the width each bar should have

        // specify the width each bar should have
        rootView.chart1.getBarData().setBarWidth(barWidth)

        // restrict the x-axis range

        // restrict the x-axis range
        rootView.chart1.getXAxis().setAxisMinimum(0F)

        // barData.getGroupWith(...) is a helper that calculates the width each group needs based on the provided parameters

        // barData.getGroupWith(...) is a helper that calculates the width each group needs based on the provided parameters
        rootView.chart1.getXAxis().setAxisMaximum(
            0 + rootView.chart1.getBarData()
                .getGroupWidth(groupSpace, barSpace) * groupCount
        )
        rootView.chart1.groupBars(0F, groupSpace, barSpace)
        rootView.chart1.invalidate()
    }


    private fun clickListener(rootView: View) {
        rootView.txt_logout.setOnClickListener {

            openLoginPage()

        }

    }

    private fun openLoginPage() {
        mainActivity?.logout()
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getAllapi() {
        mainActivity!!.apiImp.showProgressBar()

        val inveReq =
            SearchInvenReqData()
        val inveReqDTO =
            SearchInvenDTO()

        inveReq.setService_name(ApiUtils.GETBINAUDITDATA)
        inveReq.token = mainActivity!!.pref.get(AppConstants.token)

        inveReqDTO.from_date = InventoryActivity.searchDto.from_date//DataUtils.getCurrentDate()
        inveReqDTO.to_date = InventoryActivity.searchDto.to_date//DataUtils.getCurrentDate()
        inveReqDTO.interval = InventoryActivity.searchDto.interval

        inveReq.conditions = inveReqDTO

        mainActivity!!.apiImp.getBarAuditData(inveReq, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                mainActivity!!.apiImp.cancelProgressBar()
                val detail = any as InventoryBinAuditResponse
                Log.e("TAG", "onSuccess: " + detail.data.size)

                val xAxisLabel: ArrayList<String> = ArrayList()
                for (item in detail.data) {
                    xAxisLabel.add(item.chart_data.getxValue())
                }
                setdummyData(detail.data, xAxisLabel)
            }

        })
    }

}