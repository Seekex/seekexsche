package com.seekex.dashboardproject.fragments.nonscanableinventory

import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.Legend.LegendForm
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.XAxis.XAxisPosition
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.components.YAxis.YAxisLabelPosition
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.seekex.dashboardproject.AppConstants
import com.seekex.dashboardproject.R
import com.seekex.dashboardproject.activities.NonScanableInventoryActivity
import com.seekex.dashboardproject.custom.DayAxisValueFormatter
import com.seekex.dashboardproject.custom.Fill
import com.seekex.dashboardproject.custom.MyAxisValueFormatter
import com.seekex.dashboardproject.custom.XYMarkerView
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.SearchInvenDTO
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.SearchInvenReqData
import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.InventoryResponse
import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.InventoryResponse2
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import kotlinx.android.synthetic.main.homeactivity.view.*
import java.util.*


class SubLocationTransactionFragment : Fragment(), OnChartValueSelectedListener {


    private var chartTitle: String? = ""
    var dataList: ArrayList<InventoryResponse2> = arrayListOf()

    private lateinit var rootView: View
    private lateinit var dialog: Dialog

    var mainActivity: NonScanableInventoryActivity? = null

    companion object {


        @JvmStatic
        val instance: Fragment
            get() = SubLocationTransactionFragment()
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater!!.inflate(R.layout.homeactivity, container, false)
        mainActivity = activity as NonScanableInventoryActivity?
        rootView.txtname.setText(mainActivity?.pref?.get(AppConstants.name))
        clickListener(rootView)
        setChartConfig(rootView)
        getAllapi()


        return rootView
    }

    private fun setData(
        datalist: ArrayList<InventoryResponse2>,
        rootView: View,
        xaxislist: ArrayList<String>
    ) {
        val xAxis: XAxis = rootView.chart1.getXAxis()
        dataList.addAll(datalist)
        Log.e("TAG", "setData: " + xaxislist.size)

//        xAxis.valueFormatter = object : ValueFormatter() {
//            override fun getFormattedValue(value: Float, axis: AxisBase): String {
//                return xaxislist.get(value.toInt())
//            }
//        }
        val values = ArrayList<BarEntry>()
        var i: Int = 0
        for (item in datalist) {

            values.add(
                BarEntry(
                    i.toFloat(),
                    item.chart_data.getyValue().toFloat()
                )
            )
            i++
        }
        xAxis.setValueFormatter(IndexAxisValueFormatter(xaxislist));

        val set1: BarDataSet
        if (rootView.chart1.getData() != null &&
            rootView.chart1.getData().getDataSetCount() > 0
        ) {
            set1 = rootView.chart1.getData().getDataSetByIndex(0) as BarDataSet

            set1.values = values
            rootView.chart1.getData().notifyDataChanged()
            rootView.chart1.notifyDataSetChanged()


        } else {
            set1 = BarDataSet(values, chartTitle)
            set1.setDrawIcons(false)
            val startColor1 = ContextCompat.getColor(activity!!, android.R.color.holo_orange_light)
            val startColor2 = ContextCompat.getColor(activity!!, android.R.color.holo_blue_light)
            val startColor3 = ContextCompat.getColor(activity!!, android.R.color.holo_orange_light)
            val startColor4 = ContextCompat.getColor(activity!!, android.R.color.holo_green_light)
            val startColor5 = ContextCompat.getColor(activity!!, android.R.color.holo_red_light)
            val endColor1 = ContextCompat.getColor(activity!!, android.R.color.holo_blue_dark)
            val endColor2 = ContextCompat.getColor(activity!!, android.R.color.holo_purple)
            val endColor3 = ContextCompat.getColor(activity!!, android.R.color.holo_green_dark)
            val endColor4 = ContextCompat.getColor(activity!!, android.R.color.holo_red_dark)
            val endColor5 = ContextCompat.getColor(activity!!, android.R.color.holo_orange_dark)
            val gradientFills: MutableList<Fill> = ArrayList<Fill>()
            gradientFills.add(Fill(startColor1, endColor1))
            gradientFills.add(Fill(startColor2, endColor2))
            gradientFills.add(Fill(startColor3, endColor3))
            gradientFills.add(Fill(startColor4, endColor4))
            gradientFills.add(Fill(startColor5, endColor5))
//            set1.setFills(gradientFills)
            val dataSets = ArrayList<IBarDataSet>()
            dataSets.add(set1)
            val data = BarData(dataSets)
            data.setValueTextSize(10f)
            data.barWidth = 0.9f
            rootView.chart1.setData(data)
        }
    }




    private fun setChartConfig(rootView: View) {

        rootView.chart1.setDrawBarShadow(false)
        rootView.chart1.setDrawValueAboveBar(true)

        rootView.chart1.getDescription().setEnabled(false)

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        rootView.chart1.setMaxVisibleValueCount(60)

        // scaling can now only be done on x- and y-axis separately
        // scaling can now only be done on x- and y-axis separately
        rootView.chart1.setPinchZoom(false)

        rootView.chart1.setDrawGridBackground(false)
        // chart.setDrawYLabels(false);
        val xAxisFormatter: IAxisValueFormatter = DayAxisValueFormatter(rootView.chart1)

        val xAxis: XAxis = rootView.chart1.getXAxis()
        xAxis.position = XAxisPosition.BOTTOM
        xAxis.setDrawGridLines(false)
        xAxis.granularity = 1f // only intervals of 1 day

        xAxis.labelCount = 7
//        xAxis.setValueFormatter(xAxisFormatter as ValueFormatter?)

        val custom: IAxisValueFormatter = MyAxisValueFormatter()

        val leftAxis: YAxis = rootView.chart1.getAxisLeft()
        leftAxis.setLabelCount(8, false)
//        leftAxis.setValueFormatter(custom as ValueFormatter?)
        leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART)
        leftAxis.spaceTop = 15f
        leftAxis.axisMinimum = 0f // this replaces setStartAtZero(true)


        rootView.chart1.getAxisRight().isEnabled = false
//        rightAxis.setDrawGridLines(false)
//        rightAxis.setLabelCount(8, false)
////        rightAxis.setValueFormatter(custom)
//        rightAxis.spaceTop = 15f
//        rightAxis.axisMinimum = 0f // this replaces setStartAtZero(true)


        val l: Legend = rootView.chart1.getLegend()
        l.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
        l.orientation = Legend.LegendOrientation.HORIZONTAL
        l.setDrawInside(false)
        l.form = LegendForm.SQUARE
        l.formSize = 9f
        l.textSize = 11f
        l.xEntrySpace = 4f

        val mv = XYMarkerView(activity, xAxisFormatter)
        mv.setChartView(rootView.chart1) // For bounds control

        rootView.chart1.setMarker(mv) // Set the marker to the chart
    }

    private fun clickListener(rootView: View) {


    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
     fun getAllapi() {
        mainActivity!!.apiImp.showProgressBar()

        val inveReq =
            SearchInvenReqData()
        val inveReqDTO =
            SearchInvenDTO()

        inveReq.setService_name(ApiUtils.SUBLOCATIONTRANSACTION)
        inveReq.token = mainActivity!!.pref.get(AppConstants.token)

        inveReqDTO.from_date = NonScanableInventoryActivity.searchDto.from_date//DataUtils.getCurrentDate()
        inveReqDTO.to_date = NonScanableInventoryActivity.searchDto.to_date//DataUtils.getCurrentDate()
        inveReqDTO.interval = NonScanableInventoryActivity.searchDto.interval
        inveReqDTO.category_id = NonScanableInventoryActivity.searchDto.category_id

        inveReq.conditions = inveReqDTO

        mainActivity!!.apiImp.getBarData(inveReq, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                mainActivity!!.apiImp.cancelProgressBar()
                val detail = any as InventoryResponse
                Log.e("TAG", "onSuccess: " + detail.data.size)

                val xAxisLabel: ArrayList<String> = ArrayList()
                for (item in detail.data) {
                    xAxisLabel.add(item.chart_data.getxValue())
                }
                if (detail.data.size > 0) {
                    chartTitle = detail.title

                }
                setData(detail.data, rootView, xAxisLabel)
                rootView.chart1.notifyDataSetChanged()
                rootView.chart1.invalidate()

            }

        })
    }


    override fun onValueSelected(e: Entry?, h: Highlight?) {

        Toast.makeText(activity, "" + e!!.data, Toast.LENGTH_SHORT).show()
    }

    override fun onNothingSelected() {
        dialog.dismiss()

    }
}