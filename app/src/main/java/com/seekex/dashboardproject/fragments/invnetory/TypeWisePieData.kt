package com.seekex.dashboardproject.fragments.invnetory

import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.MPPointF
import com.seekex.dashboardproject.AppConstants
import com.seekex.dashboardproject.R
import com.seekex.dashboardproject.activities.InventoryActivity
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.ApiCallBacknew
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.SearchInvenDTO
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.SearchInvenReqData
import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.*
import com.seekx.webService.ApiUtils
import kotlinx.android.synthetic.main.homeactivity.view.txtname
import kotlinx.android.synthetic.main.piecharttypewise.view.*
import java.text.SimpleDateFormat
import java.util.*


class TypeWisePieData : Fragment(), ApiCallBacknew {


    private var chartTitle: String? = ""
    var dataList: ArrayList<InventoryResponse2> = arrayListOf()

    private lateinit var rootView: View

    var mainActivity: InventoryActivity? = null

    companion object {


        @JvmStatic
        val instance: Fragment
            get() = TypeWisePieData()
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater!!.inflate(R.layout.piecharttypewise, container, false)
        mainActivity = activity as InventoryActivity?
        rootView.txtname.setText(mainActivity?.pref?.get(AppConstants.name))
        clickListener(rootView)



        return rootView
    }


    private fun openPieChart(datalist: ArrayList<InventoryPieResponse.InventoryPieResponse2>) {

//        dialog = Dialog(activity!!, android.R.style.Theme_Light_NoTitleBar_Fullscreen)
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialog.setContentView(R.layout.openpiechart)
//
//        dialog.setCanceledOnTouchOutside(false)
//        dialog.setCancelable(false)
//        var chart = dialog.findViewById<PieChart>(R.id.piechart)
        rootView.piechart.setUsePercentValues(true)
        rootView.piechart.getDescription().setEnabled(false)
        rootView.piechart.setExtraOffsets(5f, 10f, 5f, 5f)

        rootView.piechart.setDragDecelerationFrictionCoef(0.95f)

        rootView.piechart.setCenterText(generateCenterSpannableText())

        rootView.piechart.setDrawHoleEnabled(true)
        rootView.piechart.setHoleColor(Color.WHITE)

        rootView.piechart.setTransparentCircleColor(Color.WHITE)
        rootView.piechart.setTransparentCircleAlpha(110)

        rootView.piechart.setHoleRadius(58f)
        rootView.piechart.setTransparentCircleRadius(61f)

        rootView.piechart.setDrawCenterText(true)

        rootView.piechart.setRotationAngle(0f)
        // enable rotation of the chart by touch
        // enable rotation of the chart by touch
        rootView.piechart.setRotationEnabled(true)
        rootView.piechart.setHighlightPerTapEnabled(true)


        rootView.piechart.animateY(1400, Easing.EaseInOutQuad)
        // chart.spin(2000, 0, 360);

        // chart.spin(2000, 0, 360);
        val l: Legend = rootView.piechart.getLegend()
        l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
        l.orientation = Legend.LegendOrientation.VERTICAL
        l.setDrawInside(false)
        l.xEntrySpace = 7f
        l.yEntrySpace = 0f
        l.yOffset = 0f

        // entry label styling

        // entry label styling
        rootView.piechart.setEntryLabelColor(Color.WHITE)
        rootView.piechart.setEntryLabelTextSize(12f)

        var total: Int = 0
        val entries = ArrayList<PieEntry>()

        for (item in datalist) {
            entries.add(PieEntry(item.value.toFloat(), item.name))
            total = item.value.toInt() + total
            Log.e("TAG", "openPieChart: " + item.value.toFloat() + " " + item.name)
        }

//        if (total < 100) {
//            var ss = 100 - total
//            entries.add(PieEntry(ss.toFloat(), "No Data"))
//
//        }
        val dataSet = PieDataSet(entries,chartTitle)

        dataSet.setDrawIcons(false)

        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0F, 40F)
        dataSet.selectionShift = 5f

        // add a lot of colors


        // add a lot of colors
        val colors = ArrayList<Int>()

//        for (c in ColorTemplate.VORDIPLOM_COLORS) colors.add(c)

//        for (c in ColorTemplate.JOYFUL_COLORS) colors.add(c)

//        for (c in ColorTemplate.COLORFUL_COLORS) colors.add(c)
//
//        for (c in ColorTemplate.LIBERTY_COLORS) colors.add(c)
//
        for (c in ColorTemplate.PASTEL_COLORS) colors.add(c)

//        colors.add(ColorTemplate.LIBERTY_COLORS())

        dataSet.colors = colors
        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter())
        data.setValueTextSize(11f)
        data.setValueTextColor(Color.WHITE)
        rootView.piechart.data = data

        // undo all highlights

        // undo all highlights
        rootView.piechart.highlightValues(null)

        rootView.piechart.invalidate()

    }


    private fun generateCenterSpannableText(): SpannableString? {
        val s = SpannableString("SevenRocks")
        s.setSpan(RelativeSizeSpan(1.7f), 0, 10, 0)
        s.setSpan(StyleSpan(Typeface.NORMAL), 0, s.length - 10, 0)
        s.setSpan(ForegroundColorSpan(Color.GRAY), 0, s.length - 10, 0)
        s.setSpan(RelativeSizeSpan(.8f), 0, s.length - 10, 0)
        s.setSpan(StyleSpan(Typeface.ITALIC), s.length - 0, s.length, 0)
        s.setSpan(ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length - 9, s.length, 0)
        return s
    }

    private fun getCurrentDate(): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val currentDate = sdf.format(Date())
        System.out.println(" C DATE is  " + currentDate)
        return currentDate
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onResume() {
        super.onResume()

        Objects.requireNonNull(mainActivity)?.apiImp?.setApiCallBacknew(this)
        getPiedata()
    }

    private fun clickListener(rootView: View) {


    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
     fun getPiedata() {
        mainActivity!!.apiImp.showProgressBar()

        val inveReq =
            SearchInvenReqData()
        val inveReqDTO =
            SearchInvenDTO()

        inveReq.setService_name(ApiUtils.GETPIEDATATYPEWISE)
        inveReq.token = mainActivity!!.pref.get(AppConstants.token)

        inveReqDTO.from_date = InventoryActivity.searchDto.from_date
        inveReqDTO.to_date = InventoryActivity.searchDto.to_date
        inveReqDTO.interval = InventoryActivity.searchDto.interval

        inveReq.conditions = inveReqDTO

        mainActivity!!.apiImp.getPiedataTypewise(inveReq)
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onSuccess(serviceName: String?, `object`: Any?) {
        Log.e("asasasasasas", "onSuccess: activity " + serviceName)

        if (serviceName.equals(ApiUtils.GETPIEDATATYPEWISE)) {
            mainActivity!!.apiImp.cancelProgressBar()
            val detail = `object` as InventoryPieResponseTypeWise
            Log.e("TAG", "onSuccess: " + detail.data.size)
            chartTitle = detail.title
            openPieChart(detail.data)

        }
    }

    override fun onFailed(msg: String?) {
        TODO("Not yet implemented")
    }


}