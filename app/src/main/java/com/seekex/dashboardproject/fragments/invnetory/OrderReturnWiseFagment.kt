package com.seekex.dashboardproject.fragments.invnetory

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.XAxis.XAxisPosition
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.seekex.dashboardproject.AppConstants
import com.seekex.dashboardproject.R
import com.seekex.dashboardproject.activities.InventoryActivity
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.ApiCallBacknew
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.SearchInvenDTO
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.SearchInvenReqData
import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.*
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import kotlinx.android.synthetic.main.homeactivity.view.*
import java.util.*

class OrderReturnWiseFagment : Fragment() {

    private var chartName: String? = ""
    var mainActivity: InventoryActivity? = null
    private lateinit var rootView: View

    companion object {


        @JvmStatic
        val instance: Fragment
            get() = OrderReturnWiseFagment()
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater!!.inflate(R.layout.homeactivity, container, false)

        mainActivity = activity as InventoryActivity?
        clickListener(rootView)
        getAllapi()
        return rootView
    }

    private fun setchatdata(
        rootView: View,
        data: ArrayList<ReturnwiseResponse2>,
        xAxisLabel: ArrayList<String>
    ) {
//        rootView.chart1.setOnChartValueSelectedListener(this)

        rootView.chart1.getDescription().setEnabled(false)

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        rootView.chart1.setMaxVisibleValueCount(40)

        // scaling can now only be done on x- and y-axis separately

        // scaling can now only be done on x- and y-axis separately
        rootView.chart1.setPinchZoom(false)

        rootView.chart1.setDrawGridBackground(false)
        rootView.chart1.setDrawBarShadow(false)

        rootView.chart1.setDrawValueAboveBar(false)
        rootView.chart1.setHighlightFullBarEnabled(false)

        // change the position of the y-labels

        // change the position of the y-labels
        val leftAxis: YAxis = rootView.chart1.getAxisLeft()
//        leftAxis.valueFormatter = MyAxisValueFormatter()
        leftAxis.axisMinimum = 0f // this replaces setStartAtZero(true)

        rootView.chart1.getAxisRight().setEnabled(false)

        val xLabels: XAxis = rootView.chart1.getXAxis()
        xLabels.position = XAxisPosition.BOTTOM

        // chart.setDrawXLabels(false);
        // chart.setDrawYLabels(false);

        // setting data

        // chart.setDrawXLabels(false);
        // chart.setDrawYLabels(false);

        val l: Legend = rootView.chart1.getLegend()
        l.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
        l.orientation = Legend.LegendOrientation.HORIZONTAL
        l.setDrawInside(false)
        l.formSize = 8f
        l.formToTextSpace = 4f
        l.xEntrySpace = 6f

        setdata(data, rootView, xAxisLabel)

    }

    private fun setdata(
        datalist: ArrayList<ReturnwiseResponse2>,
        rootView: View,
        xAxisLabel: ArrayList<String>
    ) {

        val values = ArrayList<BarEntry>()
        val xAxis: XAxis = rootView.chart1.getXAxis()
        var i: Int = 0
        for (item in datalist) {
            val val1 = item.chart_data.getyValue().carrierReturn.toFloat()
            val val2 = item.chart_data.getyValue().customerReturn.toFloat()
            values.add(
                BarEntry(
                    i.toFloat(), floatArrayOf(val1, val2)
                )
            )
            i++
        }
        xAxis.setValueFormatter(IndexAxisValueFormatter(xAxisLabel));

        val set1: BarDataSet

//        if (rootView.chart1.getData() != null &&
//            rootView.chart1.getData().getDataSetCount() > 0
//        ) {
//            set1 = rootView.chart1.getData().getDataSetByIndex(0) as BarDataSet
//            set1.values = values
//            rootView.chart1.getData().notifyDataChanged()
//            rootView.chart1.notifyDataSetChanged()
//        } else {
        set1 = BarDataSet(values, chartName)
        set1.setDrawIcons(false)
        set1.setColors(Color.RED, Color.BLACK)
        set1.stackLabels = arrayOf("Carrier Return", "Customer Return")
        val dataSets = ArrayList<IBarDataSet>()
        dataSets.add(set1)
        val data = BarData(dataSets)
//            data.setValueFormatter(MyValueFormatter())
        data.setValueTextColor(Color.WHITE)
        rootView.chart1.setData(data)
//        }

        rootView.chart1.setFitBars(true)
        rootView.chart1.invalidate()
    }

//
//    private fun getColors(): Array<Int> {
//
//        // have as many colors as stack-values per entry
//        val colors = IntArray(3)
//        System.arraycopy(ColorTemplate.MATERIAL_COLORS, 0, colors, 0, 3)
//        return colors
//    }

    private fun clickListener(rootView: View) {
        rootView.txt_logout.setOnClickListener {

            openLoginPage()

        }

    }

    private fun openLoginPage() {
        mainActivity?.logout()
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getAllapi() {
        mainActivity!!.apiImp.showProgressBar()

        val inveReq =
            SearchInvenReqData()
        val inveReqDTO =
            SearchInvenDTO()

        inveReq.setService_name(ApiUtils.ORDERRETURNWISE)
        inveReq.token = mainActivity!!.pref.get(AppConstants.token)

        inveReqDTO.from_date = InventoryActivity.searchDto.from_date//DataUtils.getCurrentDate()
        inveReqDTO.to_date = InventoryActivity.searchDto.to_date//DataUtils.getCurrentDate()
        inveReqDTO.interval = InventoryActivity.searchDto.interval

        inveReq.conditions = inveReqDTO

        mainActivity!!.apiImp.getreturnwise(inveReq, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                mainActivity!!.apiImp.cancelProgressBar()
                val detail = any as ReturnwiseFragmentResponse
                Log.e("TAG", "onSuccess: " + detail.data.size)


                val xAxisLabel: ArrayList<String> = ArrayList()
                for (item in detail.data) {
                    xAxisLabel.add(item.chart_data.getxValue())
                }
                if (detail.data.size > 0) {
                    chartName = detail.title

                }
                setchatdata(rootView, detail.data, xAxisLabel)
            }

        })
    }

}