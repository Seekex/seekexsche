package com.seekex.dashboardproject.fragments.manufacturing

import android.app.Dialog
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.Legend.LegendForm
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.XAxis.XAxisPosition
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.components.YAxis.YAxisLabelPosition
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.MPPointF
import com.seekex.dashboardproject.AppConstants
import com.seekex.dashboardproject.R
import com.seekex.dashboardproject.activities.InventoryActivity
import com.seekex.dashboardproject.activities.ManufacturingActivity
import com.seekex.dashboardproject.custom.DayAxisValueFormatter
import com.seekex.dashboardproject.custom.Fill
import com.seekex.dashboardproject.custom.MyAxisValueFormatter
import com.seekex.dashboardproject.custom.XYMarkerView
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.ApiCallBacknew
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.SearchInvenDTO
import com.seekex.dashboardproject.retrofitclasses.models.apiRequest.SearchInvenReqData
import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.InventoryPieResponse
import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.InventoryResponse
import com.seekex.dashboardproject.retrofitclasses.models.apiResponse.InventoryResponse2
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.homeactivity.view.*
import kotlinx.android.synthetic.main.openpiechart.*
import java.text.SimpleDateFormat
import java.util.*


class ManufacturingBarFragment : Fragment(), ApiCallBacknew, OnChartValueSelectedListener {


    private var piechartTitle: String? = ""
    var dataList: ArrayList<InventoryResponse2> = arrayListOf()

    private lateinit var rootView: View
    private lateinit var dialog: Dialog

    var mainActivity: ManufacturingActivity? = null

    companion object {


        @JvmStatic
        val instance: Fragment
            get() = ManufacturingBarFragment()
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater!!.inflate(R.layout.homeactivity, container, false)
        mainActivity = activity as ManufacturingActivity?
        rootView.txtname.setText(mainActivity?.pref?.get(AppConstants.name))
        clickListener(rootView)
        setChartConfig(rootView)
        getAllapi()
        return rootView
    }

    private fun setData(
        datalist: ArrayList<InventoryResponse2>,
        rootView: View,
        xaxislist: ArrayList<String>
    ) {
        val xAxis: XAxis = rootView.chart1.getXAxis()
        dataList.clear()
        dataList.addAll(datalist)
        Log.e("TAG", "setData: " + xaxislist.size)
        val values = ArrayList<BarEntry>()
        for (item in datalist) {
            values.add(
                BarEntry(
                    item.chart_data.id.toFloat(),
                    item.chart_data.getyValue().toFloat()
                )
            )
        }
        xAxis.setValueFormatter(IndexAxisValueFormatter(xaxislist));

        val set1: BarDataSet
        if (rootView.chart1.getData() != null &&
            rootView.chart1.getData().getDataSetCount() > 0
        ) {
            set1 = rootView.chart1.getData().getDataSetByIndex(0) as BarDataSet

            set1.values = values
            rootView.chart1.getData().notifyDataChanged()
            rootView.chart1.notifyDataSetChanged()


        } else {
            set1 = BarDataSet(values, " Inventory Scannable Out")
            set1.setDrawIcons(false)
            val startColor1 = ContextCompat.getColor(activity!!, android.R.color.holo_orange_light)
            val startColor2 = ContextCompat.getColor(activity!!, android.R.color.holo_blue_light)
            val startColor3 = ContextCompat.getColor(activity!!, android.R.color.holo_orange_light)
            val startColor4 = ContextCompat.getColor(activity!!, android.R.color.holo_green_light)
            val startColor5 = ContextCompat.getColor(activity!!, android.R.color.holo_red_light)
            val endColor1 = ContextCompat.getColor(activity!!, android.R.color.holo_blue_dark)
            val endColor2 = ContextCompat.getColor(activity!!, android.R.color.holo_purple)
            val endColor3 = ContextCompat.getColor(activity!!, android.R.color.holo_green_dark)
            val endColor4 = ContextCompat.getColor(activity!!, android.R.color.holo_red_dark)
            val endColor5 = ContextCompat.getColor(activity!!, android.R.color.holo_orange_dark)
            val gradientFills: MutableList<Fill> = ArrayList<Fill>()
            gradientFills.add(Fill(startColor1, endColor1))
            gradientFills.add(Fill(startColor2, endColor2))
            gradientFills.add(Fill(startColor3, endColor3))
            gradientFills.add(Fill(startColor4, endColor4))
            gradientFills.add(Fill(startColor5, endColor5))
//            set1.setFills(gradientFills)
            val dataSets = ArrayList<IBarDataSet>()
            dataSets.add(set1)
            val data = BarData(dataSets)
            data.setValueTextSize(10f)
            data.barWidth = 0.9f
            rootView.chart1.setData(data)
        }
    }

    private fun openPieChart(datalist: ArrayList<InventoryPieResponse.InventoryPieResponse2>) {
        Log.e("TAG", " in openPieChart: ")

        dialog = Dialog(activity!!, android.R.style.Theme_Light_NoTitleBar_Fullscreen)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.openpiechart)

        dialog.setCanceledOnTouchOutside(true)
        dialog.setCancelable(true)
        var chart = dialog.findViewById<PieChart>(R.id.piechart)
        chart.setUsePercentValues(true)
        chart.getDescription().setEnabled(false)
        chart.setExtraOffsets(5f, 10f, 5f, 5f)


        chart.setDragDecelerationFrictionCoef(0.95f)

        chart.setCenterText(generateCenterSpannableText())

        chart.setDrawHoleEnabled(true)
        chart.setHoleColor(Color.WHITE)

        chart.setTransparentCircleColor(Color.WHITE)
        chart.setTransparentCircleAlpha(110)

        chart.setHoleRadius(58f)
        chart.setTransparentCircleRadius(61f)

        chart.setDrawCenterText(true)

        chart.setRotationAngle(0f)
        // enable rotation of the chart by touch
        // enable rotation of the chart by touch
        chart.setRotationEnabled(true)
        chart.setHighlightPerTapEnabled(true)


        chart.animateY(1400, Easing.EaseInOutQuad)
        // chart.spin(2000, 0, 360);
        val l: Legend = chart.getLegend()
        l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
        l.orientation = Legend.LegendOrientation.VERTICAL
        l.setDrawInside(false)
        l.xEntrySpace = 7f
        l.yEntrySpace = 0f
        l.yOffset = 0f

        // entry label styling
        chart.setEntryLabelColor(Color.WHITE)
        chart.setEntryLabelTextSize(12f)

        dialog.show()

        var total: Int = 0
        val entries = ArrayList<PieEntry>()

        for (item in datalist) {
            entries.add(PieEntry(item.value.toFloat(), item.name))
            total = item.value.toInt() + total
            Log.e("TAG", "openPieChart: " + item.value.toFloat() + " " + item.name)
        }

        val dataSet = PieDataSet(entries, piechartTitle)

        dataSet.setDrawIcons(false)

        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0F, 40F)
        dataSet.selectionShift = 5f

        // add a lot of colors


        // add a lot of colors
        val colors = ArrayList<Int>()

        for (c in ColorTemplate.PASTEL_COLORS) colors.add(c)
        dataSet.colors = colors
        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter())
        data.setValueTextSize(11f)
        data.setValueTextColor(Color.WHITE)
        chart.data = data

        // undo all highlights

        // undo all highlights
        chart.highlightValues(null)

        chart.invalidate()

        dialog.img_delete.setOnClickListener {
            dialog.dismiss()
        }
    }


    private fun generateCenterSpannableText(): SpannableString? {
        val s = SpannableString("SevenRocks")
        s.setSpan(RelativeSizeSpan(1.7f), 0, 10, 0)
        s.setSpan(StyleSpan(Typeface.NORMAL), 0, s.length - 10, 0)
        s.setSpan(ForegroundColorSpan(Color.GRAY), 0, s.length - 10, 0)
        s.setSpan(RelativeSizeSpan(.8f), 0, s.length - 10, 0)
        s.setSpan(StyleSpan(Typeface.ITALIC), s.length - 0, s.length, 0)
        s.setSpan(ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length - 9, s.length, 0)
        return s
    }

    private fun getCurrentDate(): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val currentDate = sdf.format(Date())
        System.out.println(" C DATE is  " + currentDate)
        return currentDate
    }

    override fun onDestroy() {


        super.onDestroy()
    }

    private fun setChartConfig(rootView: View) {
        rootView.chart1.setOnChartValueSelectedListener(this)

        rootView.chart1.setDrawBarShadow(false)
        rootView.chart1.setDrawValueAboveBar(true)

        rootView.chart1.getDescription().setEnabled(false)

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        rootView.chart1.setMaxVisibleValueCount(60)

        // scaling can now only be done on x- and y-axis separately
        // scaling can now only be done on x- and y-axis separately
        rootView.chart1.setPinchZoom(false)

        rootView.chart1.setDrawGridBackground(false)
        // chart.setDrawYLabels(false);
        val xAxisFormatter: IAxisValueFormatter = DayAxisValueFormatter(rootView.chart1)

        val xAxis: XAxis = rootView.chart1.getXAxis()
        xAxis.position = XAxisPosition.BOTTOM
        xAxis.setDrawGridLines(false)
        xAxis.granularity = 1f // only intervals of 1 day

        xAxis.labelCount = 7
//        xAxis.setValueFormatter(xAxisFormatter as ValueFormatter?)

        val custom: IAxisValueFormatter = MyAxisValueFormatter()

        val leftAxis: YAxis = rootView.chart1.getAxisLeft()
        leftAxis.setLabelCount(8, false)
//        leftAxis.setValueFormatter(custom as ValueFormatter?)
        leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART)
        leftAxis.spaceTop = 15f
        leftAxis.axisMinimum = 0f // this replaces setStartAtZero(true)


        rootView.chart1.getAxisRight().isEnabled = false
//        rightAxis.setDrawGridLines(false)
//        rightAxis.setLabelCount(8, false)
////        rightAxis.setValueFormatter(custom)
//        rightAxis.spaceTop = 15f
//        rightAxis.axisMinimum = 0f // this replaces setStartAtZero(true)


        val l: Legend = rootView.chart1.getLegend()
        l.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
        l.orientation = Legend.LegendOrientation.HORIZONTAL
        l.setDrawInside(false)
        l.form = LegendForm.SQUARE
        l.formSize = 9f
        l.textSize = 11f
        l.xEntrySpace = 4f

        val mv = XYMarkerView(activity, xAxisFormatter)
        mv.setChartView(rootView.chart1) // For bounds control

        rootView.chart1.setMarker(mv) // Set the marker to the chart
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onResume() {
        super.onResume()

        Objects.requireNonNull(mainActivity)?.apiImp?.setApiCallBacknew(this)
    }

    private fun clickListener(rootView: View) {


    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getPiedata(fromdata: String, todate: String, type: String, subChartTitle: String) {
        piechartTitle = subChartTitle
        mainActivity!!.apiImp.showProgressBar()

        val inveReq =
            SearchInvenReqData()
        val inveReqDTO =
            SearchInvenDTO()

        inveReq.setService_name(ApiUtils.GETBARPIEDATA)
        inveReq.token = mainActivity!!.pref.get(AppConstants.token)

        inveReqDTO.from_date = fromdata
        inveReqDTO.to_date = todate
        inveReqDTO.type = type

        inveReq.conditions = inveReqDTO

        mainActivity!!.apiImp.getPiedata(inveReq)
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getAllapi() {
        mainActivity!!.apiImp.showProgressBar()

        val inveReq =
            SearchInvenReqData()
        val inveReqDTO =
            SearchInvenDTO()

        inveReq.setService_name(ApiUtils.GETBARDATAINVENTORY)
        inveReq.token = mainActivity!!.pref.get(AppConstants.token)

        inveReqDTO.from_date = InventoryActivity.searchDto.from_date//DataUtils.getCurrentDate()
        inveReqDTO.to_date = InventoryActivity.searchDto.to_date//DataUtils.getCurrentDate()
        inveReqDTO.interval = InventoryActivity.searchDto.interval

        inveReq.conditions = inveReqDTO

        mainActivity!!.apiImp.getBarData(inveReq, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                mainActivity!!.apiImp.cancelProgressBar()
                val detail = any as InventoryResponse
                Log.e("TAG", "onSuccess: " + detail.data.size)

                val xAxisLabel: ArrayList<String> = ArrayList()
                for (item in detail.data) {
                    xAxisLabel.add(item.chart_data.getxValue())
                }
                if (detail.data.size > 0) {
                    piechartTitle = detail.data.get(0).next_api.sub_chart_title

                }
                setData(detail.data, rootView, xAxisLabel)
                rootView.chart1.notifyDataSetChanged()
                rootView.chart1.invalidate()
            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onSuccess(serviceName: String?, `object`: Any?) {
        Log.e("asasasasasas", "onSuccess: activity " + serviceName)

        if (serviceName.equals(ApiUtils.GETBARDATAINVENTORY)) {



        } else if (serviceName.equals(ApiUtils.GETBARPIEDATA)) {
            mainActivity!!.apiImp.cancelProgressBar()
            val detail = `object` as InventoryPieResponse
            Log.e("TAG", "onSuccess: " + detail.data.size)
            Log.e("TAG", "in GETBARPIEDATA: " + detail.data.size)

            openPieChart(detail.data)

        }
    }

    override fun onFailed(msg: String?) {
        TODO("Not yet implemented")
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onValueSelected(e: Entry?, h: Highlight?) {

        val x = e?.x
        for (item in dataList) {
            Log.e(
                "TAG",
                "onValueSelected: " + item.chart_data.getxValue() +item.chart_data.id + " -- " + e?.x
            )

            if (item.chart_data.id.toInt() == e?.x?.toInt()) {
                Log.e("TAG", "onValueSelected: " + item.next_api.conditions.from_date)
                getPiedata(
                    item.next_api.conditions.from_date,
                    item.next_api.conditions.to_date,
                    item.next_api.conditions.type,
                    item.next_api.sub_chart_title,
                )
            }
        }
    }

    override fun onNothingSelected() {
        dialog.dismiss()

    }
}