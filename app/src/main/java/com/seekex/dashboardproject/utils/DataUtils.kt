package com.seekx.utils

import android.app.ActivityManager
import android.content.Context
import android.util.Log
import androidx.core.content.ContextCompat.getSystemService
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class DataUtils {

    companion object {
        fun getCurrentDate(): String {
            val sdf = SimpleDateFormat("yyyy-MM-dd")
            val currentDate = sdf.format(Date())
            System.out.println(" C DATE is  " + currentDate)
            return currentDate
        }

        fun getYesterdayDate(): String {
            val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val cal = Calendar.getInstance()
            cal.add(Calendar.DATE, -1)
            val yesDate = dateFormat.format(cal.time) //your formatted date here
            return yesDate
        }
        fun getDaysAgo(daysAgo: Int): Date {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DAY_OF_YEAR, -daysAgo)

            return calendar.time
        }
        fun getthreeDaysDate(): String {
            val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val cal = Calendar.getInstance()
            cal.add(Calendar.DATE, -3)
            val yesDate = dateFormat.format(cal.time) //your formatted date here
            return yesDate
        }

        fun getsevenDaysDate(): String {
            val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val cal = Calendar.getInstance()
            cal.add(Calendar.DATE, -7)
            val yesDate = dateFormat.format(cal.time) //your formatted date here
            return yesDate
        }
        fun getLastThirtyDaysStartDate(): String {
            val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val cal = Calendar.getInstance()
            cal.add(Calendar.DATE, -30)
            val yesDate = dateFormat.format(cal.time) //your formatted date here
            return yesDate
        }

        fun getLastMonthStartDate(): String {
            val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val cal = Calendar.getInstance()
            cal.add(Calendar.MONTH,-1)
            cal.set(Calendar.DAY_OF_MONTH,1)
            val yesDate = dateFormat.format(cal.time) //your formatted date here
            return yesDate
        }
        fun getLastMonthLastDate(): String {
            val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val cal = Calendar.getInstance()

            cal.set(Calendar.DAY_OF_MONTH,1)
            cal.add(Calendar.DAY_OF_MONTH,-1)

            val yesDate = dateFormat.format(cal.time) //your formatted date here
            return yesDate
        }
        fun getCurrentMonthFirstDate(): String {
            val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val cal = Calendar.getInstance()

            cal.set(Calendar.DAY_OF_MONTH,1)

            val yesDate = dateFormat.format(cal.time) //your formatted date here
            return yesDate
        }
        fun getCurrentMonthLastDate(): String {
            val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val cal = Calendar.getInstance()

            cal.set(Calendar.DAY_OF_MONTH,1)
            cal.add(Calendar.MONTH,1)
            cal.add(Calendar.DAY_OF_MONTH,-1)

            val yesDate = dateFormat.format(cal.time) //your formatted date here
            return yesDate
        }
        public fun isMyServiceRunning(context: Context, serviceClass: Class<*>): Boolean {
            val manager: ActivityManager =
                getSystemService(context, serviceClass) as ActivityManager
            for (service in manager.getRunningServices(Int.MAX_VALUE)) {
                if (serviceClass.name == service.service.getClassName()) {
                    Log.i("isMyServiceRunning?", true.toString() + "")
                    return true
                }
            }
            Log.i("isMyServiceRunning?", false.toString() + "")
            return false
        }


    }

}
