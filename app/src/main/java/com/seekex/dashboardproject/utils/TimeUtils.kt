//package com.seekx.utils
//
//import android.annotation.SuppressLint
//import android.app.TimePickerDialog
//import android.content.Context
//import android.util.Log
//
//import java.text.SimpleDateFormat
//import java.util.*
//import java.util.concurrent.TimeUnit
//import kotlin.math.roundToLong
//
//
//@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
//class TimeUtils {
//
//    companion object {
//
//        @SuppressLint("SimpleDateFormat")
//        fun checkDates(startDate: String, endDate: String):Boolean{
//
//            Log.v("checkDates", startDate)
//            Log.v("checkDates", endDate)
//
//            val dfDate  = SimpleDateFormat("yyyy-MM-dd")
//            var b = false
//            try{
//                b = when {
//                    dfDate.parse(startDate).before(dfDate.parse(endDate)) -> true //If start data is before end data
//                    dfDate.parse(startDate) == dfDate.parse(endDate) -> false //If two dates are equal
//                    else -> false //If start data is after the end data
//                }
//            }catch (e: Exception){
//                Log.v("checkDates", e.toString())
//            }
//            Log.v("checkDates", b.toString())
//            return b
//
//
//        }
//
//        @SuppressLint("SimpleDateFormat")
//        fun getTimeByDate(myDate: String):Long{
//            Log.v("getTimeByDate", myDate)
//            val sdf = SimpleDateFormat("yyyy-MM-dd")
//            val date = sdf.parse(myDate)
//            return date.time
//        }
//
//
//
//        @JvmStatic
//        fun convertInMinutes(seconds: Double): String {
//            val day = TimeUnit.SECONDS.toDays(seconds.roundToLong())
//            val hours = TimeUnit.SECONDS.toHours(seconds.roundToLong()) - day * 24
//            val minute = TimeUnit.SECONDS.toMinutes(seconds.roundToLong()) - TimeUnit.SECONDS.toHours(
//                seconds.roundToLong()
//            ) * 60
//            val second = TimeUnit.SECONDS.toSeconds(seconds.roundToLong()) - TimeUnit.SECONDS.toMinutes(
//                seconds.roundToLong()
//            ) * 60
//
//            var durationString="$day Day $hours Hour $minute Minute $second Seconds"
//
//            Log.v("convertInMinutes", durationString)
//
//            if(day == 0L)
//              durationString=durationString.replace("$day Day ", AppConstants.EMPTY)
//
//            if(hours == 0L)
//                durationString= durationString.replace("$hours Hour ", AppConstants.EMPTY)
//
//            if(minute == 0L)
//                durationString= durationString.replace("$minute Minute ", AppConstants.EMPTY)
//
//
//            Log.v("convertInMinutes", durationString)
//
//            return " |  $durationString"
//        }
//
//
//
//        fun getFormatedDuration(duration: Long?):String{
//            if(duration==null)
//                return AppConstants.NA
//            else if(duration>60){
//                return "${ExtraUtils.roundTwoDecimals(duration.toDouble() / 60)} Min"
//            }
//
//            return "$duration Sec"
//
//
//        }
//
//        @JvmStatic
//        fun getTimestamp(): String {
//            val tsLong = System.currentTimeMillis()
//            return tsLong.toString()
//        }
//
//        fun selectTime(context: Context, timeePickerCallBack: TimePickerCallBack) {
//            val c = Calendar.getInstance()
//            val hour = c.get(Calendar.HOUR)
//            val minute = c.get(Calendar.MINUTE)
//
//
//
//
//            val tpd = TimePickerDialog(
//                context,
//                R.style.themeOnverlay_timePicker,
//                TimePickerDialog.OnTimeSetListener(function = { view, h, m ->
//
//                    //  var hourString=h.toString()
//
//                    // var minuteString=m.toString()
//                    //  var amPm=" AM"
//                    var finalString = ""
//
//
//                    /* if(hourString.toInt()==0){
//                        hourString="00"
//                    }
//                    else*/ /*if(hourString.length==1){
//                        hourString="0$hourString"
//                    }*//*else if(hourString.toInt()>12){
//                        hourString=(hourString.toInt()-12).toString()
//                        amPm=" PM"
//                    }*/
//
//
//                    /*if(minuteString.length==1){
//                    minuteString="0$minuteString"
//
//                }
//                     */
//                    var mm:String
//
//                    val mintRound =m- (m % 10)
//
////                    if (m<9){
////                        mm = "0" + m
////                    }else{
//                        mm =  mintRound.toString()
////                    }
//
//                    var time:String= h.toString()+":"+mm
//                    timeePickerCallBack.onSelection(time, h,mintRound)
//                }), hour, minute, false
//            )
//
//
//
//            tpd.show()
//        }
//
//
//        @JvmStatic
//         @SuppressLint("SimpleDateFormat")
//         fun convertSecondToHHMMString(secondtTime: Long): String {
//            val df = SimpleDateFormat(AppConstants.countDownPattern)
//            df.timeZone = TimeZone.getTimeZone(AppConstants.timeZone)
//             return df.format(Date(secondtTime * 1000L))
//        }
//
//        fun timeStampToDate(timestamp: String): String {
//            val d = Date(timestamp.toLong())
//            val format = SimpleDateFormat(AppConstants.DateFormat)
//            val newDate = format.format(d)
//            Log.v("timeStampToDate", newDate)
//            return newDate
//        }
//
//        fun getDateFromString(dateString: String):Date{
//            return SimpleDateFormat(AppConstants.DateFormats).parse(dateString)
//
//        }
//
//
//        @SuppressLint("SimpleDateFormat")
//        fun formatDate(dateString: String):Date{
//            val  df =SimpleDateFormat(AppConstants.DateFormats)
//
//            /*val result = df.parse(dateString)
//            val sdf = SimpleDateFormat(AppConstants.DateFormat)
//            sdf.timeZone = TimeZone.getTimeZone("GMT")
//            val stringfromat=sdf.format(result)*/
//
//            return  df.parse(dateString)!!
//
//        }
//
//   @SuppressLint("SimpleDateFormat")
//        fun getFormattedDate(dateString: String):String{
//            val  df =SimpleDateFormat(AppConstants.DateFormats)
//            val result = df.parse(dateString)
//            val sdf = SimpleDateFormat(AppConstants.DateFormat)
//
//            val date=sdf.format(result)
//
//            val removable=date.trim().split(" ")[4]
//
//
//            return  "${date.replace(removable, "")} at $removable"
//
//        }
//
//        @SuppressLint("SimpleDateFormat")
//        fun getMilliSeconds(startCall: String): Long {
//            return SimpleDateFormat(AppConstants.DateFormats).parse(startCall).time
//        }
//
//    }
//
//}